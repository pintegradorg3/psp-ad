package Vistas;

import dao.*;
import pojos.*;

import java.sql.SQLException;
import java.util.List;

public class Service {

    private ArticuloDao articuloDao;
    private CategoriaDAO categoriaDAO;
    private UsuarioDAO usuarioDAO;
    private EtiquetaDAO etiquetaDAO;
    private EmpleadoDAO empleadoDAO;
    private MensajeDAO mensajeDAO;
    private ValoracionDAO valoracionDAO;

    public Service(){
        try {
            this.articuloDao = new ArticuloDao();
            this.categoriaDAO = new CategoriaDAO();
            this.usuarioDAO = new UsuarioDAO();
            this.empleadoDAO=new EmpleadoDAO();
            this.etiquetaDAO=new EtiquetaDAO();
            this.mensajeDAO=new MensajeDAO();
            this.valoracionDAO=new ValoracionDAO();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }



    /*
    * Service Usuarios
    */

    public List<BatoipopUsuario> buscarTodosUsuarios() throws Exception {//Funciona bien
        return  usuarioDAO.findAll();
    }

    public BatoipopUsuario buscarUsuario(int id) throws Exception {//Funciona bien

        return usuarioDAO.findByPK(id);
    }
    public boolean loginUsuario(String contrasenya,String correo){//Funciona bien
        try {
            List<BatoipopUsuario> usuarios = buscarTodosUsuarios();
            for (BatoipopUsuario batoipopUsuario : usuarios) {
                if (batoipopUsuario.getContrasenyaUsuario().equals(contrasenya) && batoipopUsuario.getCorreoUsuario().equals(correo)) {
                    return true;
                }

            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    public void anyadirUsuario(BatoipopUsuario usu) throws Exception {
        usuarioDAO.insert(usu);
    }

    public boolean actualizarUsuario(BatoipopUsuario usu) throws Exception {
        return usuarioDAO.update(usu);
    }
    public boolean borrarPorIDUsuario(int id)throws Exception{
        return empleadoDAO.delete(id);
    }

    public boolean borrarUsuario(BatoipopUsuario usu) throws Exception {
        return usuarioDAO.delete(usu);
    }
   /* public List<BatoipopArticles> articulosUsuario(BatoipopUsuario usu){
        List<BatoipopArticles> articulosFinales = new ArrayList<>();
        try {


            List<BatoipopArticles> articles = articuloDao.findAll();

            for (BatoipopArticles article : articles) {
                if (article.getBatoipopUsuarioByPublicarArticulo().equals(usu)) {
                    articulosFinales.add(article);
                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return articulosFinales;

    }*/


    public String obtenerContraseñaUsuario(BatoipopUsuario usu){
        return usu.getContrasenyaUsuario();
    }
    public String obtenerCorreoUsuario(BatoipopUsuario usu){
        return usu.getCorreoUsuario();
    }

    /*
     * Service Articulos
     */
    public List<BatoipopArticles> buscarTodosArticle()throws Exception{
        return articuloDao.findAll();
    }

    public BatoipopArticles buscarArticle(int id) throws Exception {
        return articuloDao.findByPK(id);
    }

    public boolean anyadirArticle(BatoipopArticles articles) throws Exception {
        return articuloDao.insert(articles);
    }

    public boolean actualizarArticle(BatoipopArticles articles) throws Exception {
        return articuloDao.update(articles);
    }

    public boolean borrarArticle(BatoipopArticles articles) throws Exception {
        return articuloDao.delete(articles);
    }
    public boolean borrarArticle(int id) throws Exception {
        return articuloDao.delete(id);
    }
    public BatoipopUsuario obtenerUsuarioPublicaArticle(BatoipopArticles articles){
        return articles.getBatoipopUsuarioByPublicarArticulo();
    }
    public BatoipopUsuario obtenerCompradorArticle(BatoipopArticles articles){
        return  articles.getBatoipopUsuarioByComprarArticulo();
    }



     /*
      Service Categoria
      */

    public List<BatoipopCategoria>buscarTodasCategorias() throws Exception {
        return categoriaDAO.findAll();
    }
    public BatoipopCategoria buscarCategoria(int id) throws Exception {
        return categoriaDAO.findByPK(id);
    }

    public boolean anyadirCategoria(BatoipopCategoria cat) throws Exception {
        return categoriaDAO.insert(cat);
    }

    public boolean actualizarCategoria(BatoipopCategoria cat) throws Exception {
        return categoriaDAO.update(cat);
    }

    public boolean borrarCategoria(BatoipopCategoria cat) throws Exception {
        return categoriaDAO.delete(cat);
    }
    public int obtenerIdCategoria(BatoipopCategoria cat){
        return cat.getId();

    }
    public String obtenerNombreCategoria(BatoipopCategoria cat){
        return cat.getName();

    }

    /*
    Service Empleado
    */
    public List<BatoipopEmpleados> buscarTodosEmpleado() throws Exception {
        return  empleadoDAO.findAll();
    }

    public BatoipopEmpleados buscarEmpleado(int id) throws Exception {
        return empleadoDAO.findByPK(id);
    }
    public boolean loginEmpleado(String contrasenya,String correo){
        try {


            List<BatoipopEmpleados> empleados = buscarTodosEmpleado();
            for (BatoipopEmpleados batoipopEmpleados : empleados) {
                if (batoipopEmpleados.getContrasenyaEmpleado().equals(contrasenya) && batoipopEmpleados.getCorreoEmpleado().equals(correo)) {
                    return true;
                }

            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    public boolean anyadirEmpleado(BatoipopEmpleados emp) throws Exception {
        return empleadoDAO.insert(emp);
    }

    public boolean actualizarEmpleado(BatoipopEmpleados emp) throws Exception {
        return empleadoDAO.update(emp);
    }

    public boolean borrarEmpleado(BatoipopEmpleados emp) throws Exception {
        return empleadoDAO.delete(emp);
    }
    public boolean borrarPorIDEmpleado(int id)throws Exception{
        return empleadoDAO.delete(id);
    }

    public String obtenerContraseñaEmpleado(BatoipopEmpleados emp){
        return emp.getContrasenyaEmpleado();
    }
    public String obtenerCorreoEmpleado(BatoipopEmpleados emp){
        return emp.getCorreoEmpleado();
    }

    /*
    Service Etiqueta
    */

    public BatoipopEtiqueta buscarEtiqueta(int id) throws Exception {
        return etiquetaDAO.findByPK(id);
    }
    public List<BatoipopEtiqueta> buscarTodosEtiqueta() throws Exception {//Funciona bien
        return  etiquetaDAO.findAll();
    }

    public boolean anyadirEtiqueta(BatoipopEtiqueta eti) throws Exception {
        return etiquetaDAO.insert(eti);
    }

    public boolean actualizarEtiqueta(BatoipopEtiqueta eti) throws Exception {
        return etiquetaDAO.update(eti);
    }

    public boolean borrarEtiqueta(BatoipopEtiqueta eti) throws Exception {
        return etiquetaDAO.delete(eti);
    }
    public int obtenerIdEtiqueta(BatoipopEtiqueta eti){
        return eti.getId();

    }
    public String obtenerNombreEtiqueta(BatoipopEtiqueta eti){
        return eti.getNombre();
    }
    public String obtenerDescripcionEtiqueta(BatoipopEtiqueta eti){
        return eti.getDescription();
    }

    /*
    Service Mensaje
    */

    public List<BatoipopMensaje> buscarTodosMensaje() throws Exception {//Funciona bien
        return  mensajeDAO.findAll();
    }
    public BatoipopMensaje buscarMensaje(int id) throws Exception {
        return mensajeDAO.findByPK(id);
    }

    public boolean anyadirMensaje(BatoipopMensaje mensaje) throws Exception {
        return mensajeDAO.insert(mensaje);
    }

    public boolean actualizarMensaje(BatoipopMensaje mensaje) throws Exception {
        return mensajeDAO.update(mensaje);
    }

    public boolean borrarMensaje(BatoipopMensaje mensaje) throws Exception {
        return mensajeDAO.delete(mensaje);
    }

    public int obtenerIdMensaje(BatoipopMensaje mensaje) throws Exception {
        return mensaje.getId();
    }

    public String obtenerMensajeDelMensaje(BatoipopMensaje mensaje) throws Exception {
        return mensaje.getMensaje();
    }

    public int obtenerUsuarioRecibidoMensaje(BatoipopMensaje mensaje) throws Exception {
        return mensaje.getIdUsuarioRecibido();
    }
    public int obtenerUsuarioEnviadoMensaje(BatoipopMensaje mensaje) throws Exception {
        return mensaje.getIdUsuarioEnviado();
    }
    public int obtenerArticuloMensaje(BatoipopMensaje mensaje){
        return mensaje.getArticuloIdArticulo();
    }
    public BatoipopArticles obtenerArticuloDelMensaje(BatoipopMensaje mensaje){
        List<BatoipopArticles> articulos;
        BatoipopArticles articulo = null;
        try {


            articulos = articuloDao.findAll();
            for (int i = 0; i < articulos.size(); i++) {
                if (articulos.get(i).getId()==mensaje.getArticuloIdArticulo()){
                    articulo=articulos.get(i);

                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return articulo;
    }



    /*
    Service Valoracion

    */
    public List<BatoipopValoracion> buscarTodosValoracion() throws Exception {//Funciona bien
        return  valoracionDAO.findAll();
    }
    public BatoipopValoracion buscarValoracion(int id) throws Exception {
        return valoracionDAO.findByPK(id);
    }

    public boolean anyadirValoracion(BatoipopValoracion valora) throws Exception {
        return valoracionDAO.insert(valora);
    }

    public boolean actualizarValoracion(BatoipopValoracion valora) throws Exception {
        return valoracionDAO.update(valora);
    }

    public boolean borrarValoracion(BatoipopValoracion valora) throws Exception {
        return valoracionDAO.delete(valora);
    }
    public boolean borrarValoracion(int valora) throws Exception {
        return valoracionDAO.delete(valora);
    }


}
