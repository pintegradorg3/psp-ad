package pojos;// Generated 18 feb 2022 15:24:26 by Hibernate Tools 4.3.5.Final

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class BatoipopValoracion implements java.io.Serializable {
	@SerializedName("id")
	@Expose
	private int id;
	@SerializedName("batoipopUsuario")
	@Expose
	private BatoipopUsuario batoipopUsuario;
	private Integer createUid;
	private Date createDate;
	private Integer writeUid;
	private Date writeDate;
	@SerializedName("valoracion")
	@Expose
	private String valoracion;

	public BatoipopValoracion() {
	}

	public BatoipopValoracion(int id) {
		this.id = id;
	}

	public BatoipopValoracion(int id, BatoipopUsuario batoipopUsuario, Integer createUid, Date createDate,
			Integer writeUid, Date writeDate, String valoracion) {
		this.id = id;
		this.batoipopUsuario = batoipopUsuario;
		this.createUid = createUid;
		this.createDate = createDate;
		this.writeUid = writeUid;
		this.writeDate = writeDate;
		this.valoracion = valoracion;
	}

	public BatoipopValoracion(BatoipopUsuario batoipopUsuario, String valoracion) {

		this.batoipopUsuario = batoipopUsuario;
		this.valoracion = valoracion;
	}

	public BatoipopValoracion(BatoipopUsuario batoipopUsuario) {
		this.batoipopUsuario = batoipopUsuario;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public BatoipopUsuario getBatoipopUsuario() {
		return this.batoipopUsuario;
	}

	public void setBatoipopUsuario(BatoipopUsuario batoipopUsuario) {
		this.batoipopUsuario = batoipopUsuario;
	}

	public Integer getCreateUid() {
		return this.createUid;
	}

	public void setCreateUid(Integer createUid) {
		this.createUid = createUid;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Integer getWriteUid() {
		return this.writeUid;
	}

	public void setWriteUid(Integer writeUid) {
		this.writeUid = writeUid;
	}

	public Date getWriteDate() {
		return this.writeDate;
	}

	public void setWriteDate(Date writeDate) {
		this.writeDate = writeDate;
	}

	public String getValoracion() {
		return this.valoracion;
	}

	public void setValoracion(String valoracion) {
		this.valoracion = valoracion;
	}

	@Override
	public String toString() {
		return "BatoipopValoracion{" +
				"id=" + id +
				", batoipopUsuario=" + batoipopUsuario +
				", valoracion='" + valoracion + '\'' +
				'}';
	}
}
