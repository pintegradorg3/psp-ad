package pojos;// Generated 18 feb 2022 15:24:26 by Hibernate Tools 4.3.5.Final

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class BatoipopCategoria implements java.io.Serializable {
	@SerializedName("id")
	@Expose
	private int id;
	@SerializedName("descripcionCategoria")
	@Expose
	private String descripcionCategoria;
	@SerializedName("name")
	@Expose
	private String name;

	public BatoipopCategoria() {
	}

	public BatoipopCategoria(int id, String descripcionCategoria, String name) {
		this.id = id;
		this.descripcionCategoria = descripcionCategoria;
		this.name = name;
	}

	public BatoipopCategoria(String descripcionCategoria, String name) {
		this.descripcionCategoria = descripcionCategoria;
		this.name = name;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcionCategoria() {
		return this.descripcionCategoria;
	}

	public void setDescripcionCategoria(String descripcionCategoria) {
		this.descripcionCategoria = descripcionCategoria;
	}


	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "BatoipopCategoria{" +
				"id=" + id +
				", descripcionCategoria='" + descripcionCategoria + '\'' +
				", name='" + name + '\'' +
				'}';
	}
}
