package pojos;// Generated 18 feb 2022 15:24:26 by Hibernate Tools 4.3.5.Final

import java.util.Date;

public class BatoipopFoto implements java.io.Serializable {

	private int id;
	private String nombre;
	private Integer createUid;
	private Date createDate;
	private Integer writeUid;
	private Date writeDate;

	public BatoipopFoto() {
	}

	public BatoipopFoto(int id) {
		this.id = id;
	}

	public BatoipopFoto(String nombre) {
		this.nombre = nombre;
	}

	public BatoipopFoto(int id, String nombre, Integer createUid, Date createDate, Integer writeUid, Date writeDate) {
		this.id = id;
		this.nombre = nombre;
		this.createUid = createUid;
		this.createDate = createDate;
		this.writeUid = writeUid;
		this.writeDate = writeDate;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getCreateUid() {
		return this.createUid;
	}

	public void setCreateUid(Integer createUid) {
		this.createUid = createUid;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Integer getWriteUid() {
		return this.writeUid;
	}

	public void setWriteUid(Integer writeUid) {
		this.writeUid = writeUid;
	}

	public Date getWriteDate() {
		return this.writeDate;
	}

	public void setWriteDate(Date writeDate) {
		this.writeDate = writeDate;
	}

}
