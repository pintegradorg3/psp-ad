package pojos;// Generated 18 feb 2022 15:24:26 by Hibernate Tools 4.3.5.Final

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BatoipopArticles implements java.io.Serializable {
	@SerializedName("id")
	@Expose
	private int id;
	@SerializedName("batoipopCategoria")
	@Expose
	private BatoipopCategoria batoipopCategoria;
	@SerializedName("batoipopUsuarioByPublicarArticulo")
	@Expose
	private BatoipopUsuario batoipopUsuarioByPublicarArticulo;		 	// usuario que publica el articulo
	@SerializedName("batoipopUsuarioByComprarArticulo")
	@Expose
	private BatoipopUsuario batoipopUsuarioByComprarArticulo; 			// usuario que compra el articulo
	@SerializedName("descripcionArticulo")
	@Expose
	private String descripcionArticulo;
	@SerializedName("precioArticulo")
	@Expose
	private Double precioArticulo;
	@SerializedName("disponible")
	@Expose
	private Boolean disponible;
	@SerializedName("name")
	@Expose
	private String name; 												// nombre del articulo

	public BatoipopArticles() {
	}

	public BatoipopArticles(int id, BatoipopCategoria batoipopCategoria,
							BatoipopUsuario batoipopUsuarioByPublicarArticulo, String descripcionArticulo, String name) {
		this.id = id;
		this.batoipopCategoria = batoipopCategoria;
		this.batoipopUsuarioByPublicarArticulo = batoipopUsuarioByPublicarArticulo;
		this.descripcionArticulo = descripcionArticulo;
		this.name = name;
	}

	public BatoipopArticles(BatoipopCategoria batoipopCategoria,
			BatoipopUsuario batoipopUsuarioByPublicarArticulo, String descripcionArticulo, String name) {
		this.batoipopCategoria = batoipopCategoria;
		this.batoipopUsuarioByPublicarArticulo = batoipopUsuarioByPublicarArticulo;
		this.descripcionArticulo = descripcionArticulo;
		this.name = name;
	}

	public BatoipopArticles(BatoipopCategoria batoipopCategoria, BatoipopUsuario batoipopUsuarioByPublicarArticulo, BatoipopUsuario batoipopUsuarioByComprarArticulo, String descripcionArticulo, Double precioArticulo, Boolean disponible, String name) {
		this.batoipopCategoria = batoipopCategoria;
		this.batoipopUsuarioByPublicarArticulo = batoipopUsuarioByPublicarArticulo;
		this.batoipopUsuarioByComprarArticulo = batoipopUsuarioByComprarArticulo;
		this.descripcionArticulo = descripcionArticulo;
		this.precioArticulo = precioArticulo;
		this.disponible = disponible;
		this.name = name;
	}

	public BatoipopArticles(BatoipopCategoria batoipopCategoria,
							BatoipopUsuario batoipopUsuarioByPublicarArticulo, String descripcionArticulo, Double precioArticulo,
							Boolean disponible, String name) {
		this.batoipopCategoria = batoipopCategoria;
		this.batoipopUsuarioByPublicarArticulo = batoipopUsuarioByPublicarArticulo;
		this.descripcionArticulo = descripcionArticulo;
		this.precioArticulo = precioArticulo;
		this.disponible = disponible;
		this.name = name;
	}

	public BatoipopArticles(int id, BatoipopCategoria batoipopCategoria, BatoipopUsuario batoipopUsuarioByPublicarArticulo,
							String descripcionArticulo, double precioArticulo, boolean disponible, String name) {
		this.id = id;
		this.batoipopCategoria = batoipopCategoria;
		this.batoipopUsuarioByPublicarArticulo = batoipopUsuarioByPublicarArticulo;
		this.batoipopUsuarioByComprarArticulo = null;
		this.descripcionArticulo = descripcionArticulo;
		this.precioArticulo = precioArticulo;
		this.name = name;
		this.disponible = disponible;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public BatoipopCategoria getBatoipopCategoria() {
		return this.batoipopCategoria;
	}

	public void setBatoipopCategoria(BatoipopCategoria batoipopCategoria) {
		this.batoipopCategoria = batoipopCategoria;
	}

	public BatoipopUsuario getBatoipopUsuarioByPublicarArticulo() {
		return this.batoipopUsuarioByPublicarArticulo;
	}

	public void setBatoipopUsuarioByPublicarArticulo(BatoipopUsuario batoipopUsuarioByPublicarArticulo) {
		this.batoipopUsuarioByPublicarArticulo = batoipopUsuarioByPublicarArticulo;
	}

	public BatoipopUsuario getBatoipopUsuarioByComprarArticulo() {
		return this.batoipopUsuarioByComprarArticulo;
	}

	public void setBatoipopUsuarioByComprarArticulo(BatoipopUsuario batoipopUsuarioByComprarArticulo) {
		this.batoipopUsuarioByComprarArticulo = batoipopUsuarioByComprarArticulo;
	}

	public String getDescripcionArticulo() {
		return this.descripcionArticulo;
	}

	public void setDescripcionArticulo(String descripcionArticulo) {
		this.descripcionArticulo = descripcionArticulo;
	}

	public Double getPrecioArticulo() {
		return this.precioArticulo;
	}

	public void setPrecioArticulo(Double precioArticulo) {
		this.precioArticulo = precioArticulo;
	}

	public Boolean getDisponible() {
		return this.disponible;
	}

	public void setDisponible(Boolean disponible) {
		this.disponible = disponible;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "BatoipopArticles{" +
				"id=" + id +
				", batoipopCategoria=" + batoipopCategoria +
				", batoipopUsuarioByPublicarArticulo=" + batoipopUsuarioByPublicarArticulo +
				", batoipopUsuarioByComprarArticulo=" + batoipopUsuarioByComprarArticulo +
				", descripcionArticulo='" + descripcionArticulo + '\'' +
				", precioArticulo=" + precioArticulo +
				", disponible=" + disponible +
				", name='" + name + '\'' +
				'}';
	}
}
