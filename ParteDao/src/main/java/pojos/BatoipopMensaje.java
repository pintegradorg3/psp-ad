package pojos;// Generated 18 feb 2022 15:24:26 by Hibernate Tools 4.3.5.Final

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;


public class BatoipopMensaje implements java.io.Serializable {
	@SerializedName("id")
	@Expose
	private int id;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("mensaje")
	@Expose
	private String mensaje;
	@SerializedName("idUsuarioEnviado")
	@Expose
	private int idUsuarioEnviado;
	@SerializedName("articuloIdArticulo")
	@Expose
	private int articuloIdArticulo;
	@SerializedName("idUsuarioRecibido")
	@Expose
	private int idUsuarioRecibido;

	private Integer createUid;
	private Date createDate;
	private Integer writeUid;
	private Date writeDate;


	public BatoipopMensaje() {
	}

	public BatoipopMensaje(int id, String mensaje, int idUsuarioEnviado, int articuloIdArticulo,
			int idUsuarioRecibido,String name) {
		this.id = id;
		this.mensaje = mensaje;
		this.idUsuarioEnviado = idUsuarioEnviado;
		this.articuloIdArticulo = articuloIdArticulo;
		this.idUsuarioRecibido = idUsuarioRecibido;
		this.name=name;
	}

	public BatoipopMensaje(String mensaje, int idUsuarioEnviado, int articuloIdArticulo,int idUsuarioRecibido,String name) {
		this.mensaje = mensaje;
		this.idUsuarioEnviado = idUsuarioEnviado;
		this.articuloIdArticulo = articuloIdArticulo;
		this.idUsuarioRecibido=idUsuarioRecibido;
		this.name=name;
	}

	public BatoipopMensaje(int id, String mensaje, int idUsuarioEnviado, int articuloIdArticulo, Integer createUid,
						   Date createDate, Integer writeUid, Date writeDate, int idUsuarioRecibido,String name) {
		this.id = id;
		this.mensaje = mensaje;
		this.idUsuarioEnviado = idUsuarioEnviado;
		this.articuloIdArticulo = articuloIdArticulo;
		this.createUid = createUid;
		this.createDate = createDate;
		this.writeUid = writeUid;
		this.writeDate = writeDate;
		this.idUsuarioRecibido = idUsuarioRecibido;
		this.name=name;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMensaje() {
		return this.mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public int getIdUsuarioEnviado() {
		return this.idUsuarioEnviado;
	}

	public void setIdUsuarioEnviado(int idUsuarioEnviado) {
		this.idUsuarioEnviado = idUsuarioEnviado;
	}

	public int getArticuloIdArticulo() {
		return this.articuloIdArticulo;
	}

	public void setArticuloIdArticulo(int articuloIdArticulo) {
		this.articuloIdArticulo = articuloIdArticulo;
	}

	public Integer getCreateUid() {
		return this.createUid;
	}

	public void setCreateUid(Integer createUid) {
		this.createUid = createUid;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Integer getWriteUid() {
		return this.writeUid;
	}

	public void setWriteUid(Integer writeUid) {
		this.writeUid = writeUid;
	}

	public Date getWriteDate() {
		return this.writeDate;
	}

	public void setWriteDate(Date writeDate) {
		this.writeDate = writeDate;
	}

	public int getIdUsuarioRecibido() {
		return this.idUsuarioRecibido;
	}

	public void setIdUsuarioRecibido(int idUsuarioRecibido) {
		this.idUsuarioRecibido = idUsuarioRecibido;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "BatoipopMensaje{" +
				"id=" + id +
				", mensaje='" + mensaje + '\'' +
				", idUsuarioEnviado=" + idUsuarioEnviado +
				", articuloIdArticulo=" + articuloIdArticulo +
				", idUsuarioRecibido=" + idUsuarioRecibido +
				'}';
	}
}
