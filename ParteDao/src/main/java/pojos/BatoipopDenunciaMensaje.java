package pojos;// Generated 18 feb 2022 15:24:26 by Hibernate Tools 4.3.5.Final

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class BatoipopDenunciaMensaje implements java.io.Serializable {
	@SerializedName("id")
	@Expose
	private int id;
	@SerializedName("descriptionDenunciaM")
	@Expose
	private String descriptionDenunciaM;
	private Integer createUid;
	private Date createDate;
	private Integer writeUid;
	private Date writeDate;

	public BatoipopDenunciaMensaje() {
	}

	public BatoipopDenunciaMensaje(int id) {
		this.id = id;
	}

	public BatoipopDenunciaMensaje(int id, String descriptionDenunciaM, Integer createUid, Date createDate,
			Integer writeUid, Date writeDate) {
		this.id = id;
		this.descriptionDenunciaM = descriptionDenunciaM;
		this.createUid = createUid;
		this.createDate = createDate;
		this.writeUid = writeUid;
		this.writeDate = writeDate;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescriptionDenunciaM() {
		return this.descriptionDenunciaM;
	}

	public void setDescriptionDenunciaM(String descriptionDenunciaM) {
		this.descriptionDenunciaM = descriptionDenunciaM;
	}

	public Integer getCreateUid() {
		return this.createUid;
	}

	public void setCreateUid(Integer createUid) {
		this.createUid = createUid;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Integer getWriteUid() {
		return this.writeUid;
	}

	public void setWriteUid(Integer writeUid) {
		this.writeUid = writeUid;
	}

	public Date getWriteDate() {
		return this.writeDate;
	}

	public void setWriteDate(Date writeDate) {
		this.writeDate = writeDate;
	}

	@Override
	public String toString() {
		return "BatoipopDenunciaMensaje{" +
				"id=" + id +
				", descriptionDenunciaM='" + descriptionDenunciaM + '\'' +
				'}';
	}
}
