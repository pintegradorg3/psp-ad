package pojos;// Generated 18 feb 2022 15:24:26 by Hibernate Tools 4.3.5.Final

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class BatoipopEmpleados implements java.io.Serializable {
	@SerializedName("id")
	@Expose
	private int id;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("apellidosEmpleado")
	@Expose
	private String apellidosEmpleado;
	@SerializedName("contrasenyaEmpleado")
	@Expose
	private String contrasenyaEmpleado;
	@SerializedName("telefonoEmpleado")
	@Expose
	private String telefonoEmpleado;
	@SerializedName("correoEmpleado")
	@Expose
	private String correoEmpleado;
	@SerializedName("direccionEmpleado")
	@Expose
	private String direccionEmpleado;
	@SerializedName("poblacion")
	@Expose
	private String poblacion;

	private Integer createUid;

	private Date createDate;

	private Integer writeUid;

	private Date writeDate;

	public BatoipopEmpleados() {
	}

	public BatoipopEmpleados(int id, String name, String apellidosEmpleado, String contrasenyaEmpleado,
			String telefonoEmpleado, String correoEmpleado, String direccionEmpleado) {
		this.id = id;
		this.name = name;
		this.apellidosEmpleado = apellidosEmpleado;
		this.contrasenyaEmpleado = contrasenyaEmpleado;
		this.telefonoEmpleado = telefonoEmpleado;
		this.correoEmpleado = correoEmpleado;
		this.direccionEmpleado = direccionEmpleado;
	}

	public BatoipopEmpleados(String name, String apellidosEmpleado, String contrasenyaEmpleado, String telefonoEmpleado, String correoEmpleado, String direccionEmpleado, String poblacion) {
		this.name = name;
		this.apellidosEmpleado = apellidosEmpleado;
		this.contrasenyaEmpleado = contrasenyaEmpleado;
		this.telefonoEmpleado = telefonoEmpleado;
		this.correoEmpleado = correoEmpleado;
		this.direccionEmpleado = direccionEmpleado;
		this.poblacion = poblacion;
	}

	public BatoipopEmpleados(int id, String name, String apellidosEmpleado, String contrasenyaEmpleado,
							 String telefonoEmpleado, String correoEmpleado, String direccionEmpleado, String poblacion,
							 Integer createUid, Date createDate, Integer writeUid, Date writeDate) {
		this.id = id;
		this.name = name;
		this.apellidosEmpleado = apellidosEmpleado;
		this.contrasenyaEmpleado = contrasenyaEmpleado;
		this.telefonoEmpleado = telefonoEmpleado;
		this.correoEmpleado = correoEmpleado;
		this.direccionEmpleado = direccionEmpleado;
		this.poblacion = poblacion;
		this.createUid = createUid;
		this.createDate = createDate;
		this.writeUid = writeUid;
		this.writeDate = writeDate;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getApellidosEmpleado() {
		return this.apellidosEmpleado;
	}

	public void setApellidosEmpleado(String apellidosEmpleado) {
		this.apellidosEmpleado = apellidosEmpleado;
	}

	public String getContrasenyaEmpleado() {
		return this.contrasenyaEmpleado;
	}

	public void setContrasenyaEmpleado(String contrasenyaEmpleado) {
		this.contrasenyaEmpleado = contrasenyaEmpleado;
	}

	public String getTelefonoEmpleado() {
		return this.telefonoEmpleado;
	}

	public void setTelefonoEmpleado(String telefonoEmpleado) {
		this.telefonoEmpleado = telefonoEmpleado;
	}

	public String getCorreoEmpleado() {
		return this.correoEmpleado;
	}

	public void setCorreoEmpleado(String correoEmpleado) {
		this.correoEmpleado = correoEmpleado;
	}

	public String getDireccionEmpleado() {
		return this.direccionEmpleado;
	}

	public void setDireccionEmpleado(String direccionEmpleado) {
		this.direccionEmpleado = direccionEmpleado;
	}

	public String getPoblacion() {
		return this.poblacion;
	}

	public void setPoblacion(String poblacion) {
		this.poblacion = poblacion;
	}

	public Integer getCreateUid() {
		return this.createUid;
	}

	public void setCreateUid(Integer createUid) {
		this.createUid = createUid;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Integer getWriteUid() {
		return this.writeUid;
	}

	public void setWriteUid(Integer writeUid) {
		this.writeUid = writeUid;
	}

	public Date getWriteDate() {
		return this.writeDate;
	}

	public void setWriteDate(Date writeDate) {
		this.writeDate = writeDate;
	}

	@Override
	public String toString() {
		return "BatoipopEmpleados{" +
				"id=" + id +
				", name='" + name + '\'' +
				", apellidosEmpleado='" + apellidosEmpleado + '\'' +
				", contrasenyaEmpleado='" + contrasenyaEmpleado + '\'' +
				", telefonoEmpleado='" + telefonoEmpleado + '\'' +
				", correoEmpleado='" + correoEmpleado + '\'' +
				", direccionEmpleado='" + direccionEmpleado + '\'' +
				", poblacion='" + poblacion + '\'' +
				'}';
	}
}
