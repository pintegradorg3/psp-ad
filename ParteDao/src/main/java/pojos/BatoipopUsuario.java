package pojos;// Generated 18 feb 2022 15:24:26 by Hibernate Tools 4.3.5.Final

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class BatoipopUsuario implements java.io.Serializable {
	@SerializedName("id")
	@Expose
	private int id;
	@SerializedName("contrasenyaUsuario")
	@Expose
	private String contrasenyaUsuario;
	@SerializedName("telefonoUsuario")
	@Expose
	private String telefonoUsuario;
	@SerializedName("nombreUsuario")
	@Expose
	private String nombreUsuario;
	@SerializedName("apellidosUsuario")
	@Expose
	private String apellidosUsuario;
	@SerializedName("correoUsuario")
	@Expose
	private String correoUsuario;
	@SerializedName("codigoPostal")
	@Expose
	private String codigoPostal;
	@SerializedName("direccionUsuario")
	@Expose
	private String direccionUsuario;
	@SerializedName("poblacion")
	@Expose
	private String poblacion;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("batoipopValoracions")
	@Expose
	private Set batoipopValoracions = new HashSet<BatoipopValoracion>(0); // Lista de valoraciones
	@SerializedName("batoipopArticlesesForPublicarArticulo")
	@Expose
	private Set batoipopArticlesesForPublicarArticulo = new HashSet(0);
	@SerializedName("batoipopArticlesesForComprarArticulo")
	@Expose
	private Set batoipopArticlesesForComprarArticulo = new HashSet(0);
	@SerializedName("batoipopArticlesesForUsuario")
	@Expose
	private Set batoipopArticlesesForUsuario = new HashSet(0);

	public BatoipopUsuario() {
	}

	public BatoipopUsuario(int id, String contrasenyaUsuario, String telefonoUsuario, String nombreUsuario,
			String apellidosUsuario, String correoUsuario, String codigoPostal, String direccionUsuario, String name) {
		this.id = id;
		this.contrasenyaUsuario = contrasenyaUsuario;
		this.telefonoUsuario = telefonoUsuario;
		this.nombreUsuario = nombreUsuario;
		this.apellidosUsuario = apellidosUsuario;
		this.correoUsuario = correoUsuario;
		this.codigoPostal = codigoPostal;
		this.direccionUsuario = direccionUsuario;
		this.name = name;
	}

	public BatoipopUsuario(String contrasenyaUsuario, String telefonoUsuario, String nombreUsuario,
			String apellidosUsuario, String correoUsuario, String codigoPostal, String direccionUsuario,
			String poblacion, String name) {
		this.contrasenyaUsuario = contrasenyaUsuario;
		this.telefonoUsuario = telefonoUsuario;
		this.nombreUsuario = nombreUsuario;
		this.apellidosUsuario = apellidosUsuario;
		this.correoUsuario = correoUsuario;
		this.codigoPostal = codigoPostal;
		this.direccionUsuario = direccionUsuario;
		this.poblacion = poblacion;
		this.name = name;
	}

	public BatoipopUsuario(int id, String contrasenyaUsuario, String telefonoUsuario, String nombreUsuario, String apellidosUsuario, String correoUsuario, String codigoPostal, String direccionUsuario, String poblacion, String name) {
		this.contrasenyaUsuario = contrasenyaUsuario;
		this.telefonoUsuario = telefonoUsuario;
		this.nombreUsuario = nombreUsuario;
		this.apellidosUsuario = apellidosUsuario;
		this.correoUsuario = correoUsuario;
		this.codigoPostal = codigoPostal;
		this.direccionUsuario = direccionUsuario;
		this.poblacion = poblacion;
		this.name = name;
		this.id = id;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public String getContrasenyaUsuario() {
		return this.contrasenyaUsuario;
	}

	public void setContrasenyaUsuario(String contrasenyaUsuario) {
		this.contrasenyaUsuario = contrasenyaUsuario;
	}

	public String getTelefonoUsuario() {
		return this.telefonoUsuario;
	}

	public void setTelefonoUsuario(String telefonoUsuario) {
		this.telefonoUsuario = telefonoUsuario;
	}


	public String getNombreUsuario() {
		return this.nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}


	public String getApellidosUsuario() {
		return this.apellidosUsuario;
	}

	public void setApellidosUsuario(String apellidosUsuario) {
		this.apellidosUsuario = apellidosUsuario;
	}


	public String getCorreoUsuario() {
		return this.correoUsuario;
	}

	public void setCorreoUsuario(String correoUsuario) {
		this.correoUsuario = correoUsuario;
	}

	public String getCodigoPostal() {
		return this.codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getDireccionUsuario() {
		return this.direccionUsuario;
	}

	public void setDireccionUsuario(String direccionUsuario) {
		this.direccionUsuario = direccionUsuario;
	}

	public String getPoblacion() {
		return this.poblacion;
	}

	public void setPoblacion(String poblacion) {
		this.poblacion = poblacion;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set getBatoipopArticlesesForPublicarArticulo() {
		return this.batoipopArticlesesForPublicarArticulo;
	}

	public void setBatoipopArticlesesForPublicarArticulo(Set batoipopArticlesesForPublicarArticulo) {
		this.batoipopArticlesesForPublicarArticulo = batoipopArticlesesForPublicarArticulo;
	}

	public Set getBatoipopValoracions() {
		return this.batoipopValoracions;
	}

	public void setBatoipopValoracions(Set batoipopValoracions) {
		this.batoipopValoracions = batoipopValoracions;
	}

	public Set getBatoipopArticlesesForComprarArticulo() {
		return this.batoipopArticlesesForComprarArticulo;
	}

	public void setBatoipopArticlesesForComprarArticulo(Set batoipopArticlesesForComprarArticulo) {
		this.batoipopArticlesesForComprarArticulo = batoipopArticlesesForComprarArticulo;
	}

	public Set getBatoipopArticlesesForUsuario() {
		return this.batoipopArticlesesForUsuario;
	}

	public void setBatoipopArticlesesForUsuario(Set batoipopArticlesesForUsuario) {
		this.batoipopArticlesesForUsuario = batoipopArticlesesForUsuario;
	}

	@Override
	public String toString() {
		return "BatoipopUsuario{" +
				"id=" + id +
				", contrasenyaUsuario='" + contrasenyaUsuario + '\'' +
				", telefonoUsuario='" + telefonoUsuario + '\'' +
				", nombreUsuario='" + nombreUsuario + '\'' +
				", apellidosUsuario='" + apellidosUsuario + '\'' +
				", correoUsuario='" + correoUsuario + '\'' +
				", codigoPostal='" + codigoPostal + '\'' +
				", direccionUsuario='" + direccionUsuario + '\'' +
				", poblacion='" + poblacion + '\'' +
				", name='" + name + '\'' +
				", batoipopValoracions=" + batoipopValoracions +
				", batoipopArticlesesForPublicarArticulo=" + batoipopArticlesesForPublicarArticulo +
				", batoipopArticlesesForComprarArticulo=" + batoipopArticlesesForComprarArticulo +
				", batoipopArticlesesForUsuario=" + batoipopArticlesesForUsuario +
				'}';
	}
}
