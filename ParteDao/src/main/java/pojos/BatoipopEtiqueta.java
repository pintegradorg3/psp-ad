package pojos;// Generated 18 feb 2022 15:24:26 by Hibernate Tools 4.3.5.Final

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class BatoipopEtiqueta implements java.io.Serializable {
	@SerializedName("id")
	@Expose
	private int id;
	@SerializedName("nombre")
	@Expose
	private String nombre;
	@SerializedName("description")
	@Expose
	private String description;

	private Integer createUid;
	private Date createDate;
	private Integer writeUid;
	private Date writeDate;

	public BatoipopEtiqueta() {
	}

	public BatoipopEtiqueta(String nombre, String description) {
		this.nombre = nombre;
		this.description = description;
	}

	public BatoipopEtiqueta(int id, String nombre, String description) {
		this.id = id;
		this.nombre = nombre;
		this.description = description;
	}

	public BatoipopEtiqueta(int id, String nombre, String description, Integer createUid, Date createDate,
			Integer writeUid, Date writeDate) {
		this.id = id;
		this.nombre = nombre;
		this.description = description;
		this.createUid = createUid;
		this.createDate = createDate;
		this.writeUid = writeUid;
		this.writeDate = writeDate;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getCreateUid() {
		return this.createUid;
	}

	public void setCreateUid(Integer createUid) {
		this.createUid = createUid;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Integer getWriteUid() {
		return this.writeUid;
	}

	public void setWriteUid(Integer writeUid) {
		this.writeUid = writeUid;
	}

	public Date getWriteDate() {
		return this.writeDate;
	}

	public void setWriteDate(Date writeDate) {
		this.writeDate = writeDate;
	}

	@Override
	public String toString() {
		return "BatoipopEtiqueta{" +
				"id=" + id +
				", nombre='" + nombre + '\'' +
				", description='" + description + '\'' +
				'}';
	}
}
