package dao;

import com.google.gson.Gson;
import pojos.BatoipopArticles;
import pojos.BatoipopCategoria;
import pojos.BatoipopEmpleados;
import pojos.BatoipopEtiqueta;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CategoriaDAO implements GenericDao.GenericDAO<BatoipopCategoria> {
    final static String HTTP = "http://localhost:8080/categoria/";

 /*   final String SQLSELECTALL = "SELECT * FROM batoipop_categoria ";
    final String SQLSELECTPK = "SELECT * FROM batoipop_categoria WHERE id = ?";

    // final String SQLSELECTALLARTICLESUSUARIOPUBLICADO = "SELECT * FROM batoipop_categoria WHERE batoipopUsuarioByPublicarArticulo = ?";

    final String SQLINSERT = "INSERT INTO batoipop_categoria (descripcionCategoria, name) VALUES (?, ?)";
    final String SQLUPDATE = "UPDATE batoipop_categoria SET descripcionCategoria = ?, name = ?  WHERE id = ?";
    final String SQLDELETE = "DELETE FROM batoipop_categoria WHERE id = ?";
    final String SQLCOUNT = "SELECT COUNT(*) FROM batoipop_categoria";


    final String SQLSELECTEXAMPLE = "SELECT * FROM batoipop_categoria where name like ? and precio <= ? and grupo like (?)";
    final String SQLSELECTEXAMPLE2 = "SELECT * FROM batoipop_categoria where name like ? and grupo like (?)";
*/
   /* private final PreparedStatement pstSelectPK;
    private final PreparedStatement pstInsertKey;
    private final PreparedStatement pstSelectAll;
  //  private final PreparedStatement pstSelectGrupo;
    private final PreparedStatement pstInsert;
    private final PreparedStatement pstUpdate;
    private final PreparedStatement pstDelete;
    private final PreparedStatement pstCount;
    private final PreparedStatement pstSelectExample;
    private final PreparedStatement pstSelectExample2;
*/

    public CategoriaDAO() throws SQLException {
     /*   Connection con = ConexionBD.getConexion();
        pstSelectPK = con.prepareStatement(SQLSELECTPK);
        pstSelectAll = con.prepareStatement(SQLSELECTALL);
        pstInsert = con.prepareStatement(SQLINSERT);
        pstUpdate = con.prepareStatement(SQLUPDATE);
        pstDelete = con.prepareStatement(SQLDELETE);
        pstCount = con.prepareStatement(SQLCOUNT);
    //    pstSelectGrupo = con.prepareStatement(SQLSELECTALLARTICLESUSUARIOPUBLICADO);
        pstSelectExample = con.prepareStatement(SQLSELECTEXAMPLE);
        pstInsertKey = con.prepareStatement(SQLINSERT,PreparedStatement.RETURN_GENERATED_KEYS);
        pstSelectExample2 = con.prepareStatement(SQLSELECTEXAMPLE2);*/
    }

    public void cerrar() throws SQLException {
       /* pstSelectPK.close();
        pstSelectAll.close();
        pstInsert.close();
        pstUpdate.close();
        pstDelete.close();*/
    }

    @Override // OKI
    public BatoipopCategoria findByPK(int id) throws Exception {
        /*BatoipopCategoria b = null;
        pstSelectPK.setInt(1, id);
        ResultSet rs = pstSelectPK.executeQuery();
        if(rs.next()){
            b = new BatoipopCategoria(id, rs.getString("descripcionCategoria"), rs.getString("name"));
        }
        rs.close();
        return b;*/
        String lin,salida="";
        List<BatoipopCategoria> listaEmpleados=null;
        BatoipopCategoria categoria = null;
        Gson gson;
        URL url;
        HttpURLConnection con;
        try {
            url=new URL(HTTP+id);
            con=(HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Accept","application/json");

            if (con.getResponseCode()!=HttpURLConnection.HTTP_OK){
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            }else {
                BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
                while ((lin=br.readLine())!=null){
                    salida=salida.concat(lin);
                }
                gson=new Gson();
                categoria=gson.fromJson(salida,BatoipopCategoria.class);
                con.disconnect();

            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        return categoria;

    }

    @Override // OKI
    public List<BatoipopCategoria> findAll() throws Exception {
        /*List<BatoipopCategoria> batoipopCategoria = new ArrayList<>();

        ResultSet rs = pstSelectAll.executeQuery();

        while (rs.next()) {
            batoipopCategoria.add(new BatoipopCategoria(rs.getInt("id"), rs.getString("descripcionCategoria"), rs.getString("name")));
        }
        rs.close();
        return batoipopCategoria;*/
        String lin,salida="";
        List<BatoipopCategoria> listaEmpleados=null;
        BatoipopCategoria[] categoria;
        Gson gson;
        URL url;
        HttpURLConnection con;
        try {
            url=new URL(HTTP);
            con=(HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Accept","application/json");
            if (con.getResponseCode()!=HttpURLConnection.HTTP_OK){
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            }else {
                BufferedReader br =new BufferedReader(new InputStreamReader(con.getInputStream()));
                while ((lin=br.readLine())!=null){
                    salida=salida.concat(lin);
                }
                gson=new Gson();
                categoria=gson.fromJson(salida,BatoipopCategoria[].class);
                listaEmpleados=new ArrayList<BatoipopCategoria>();
                listaEmpleados= Arrays.asList(categoria);
                con.disconnect();

            }

        } catch (IOException | RuntimeException e) {
            e.printStackTrace();
        }
        return listaEmpleados;

    }

    @Override
    public boolean insert(BatoipopCategoria t) throws Exception {
       /* pstInsert.setString(1, t.getDescripcionCategoria());
        pstInsert.setString(2, t.getName());

        int insertados = pstInsert.executeUpdate();
        return (insertados == 1);*/

        URL url ;
        HttpURLConnection con;
        Gson gson=new Gson();
        String empleadoJson= gson.toJson(t, BatoipopCategoria.class);

        try {
            url=new URL(HTTP);
            con = (HttpURLConnection) url.openConnection();
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");

            OutputStream os =con.getOutputStream();
            os.write(empleadoJson.getBytes());
            os.flush();
            if (con.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
                return false;
                //throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            }




        } catch (Exception e) {
            return false;
            //e.printStackTrace();
        }
        return true;
    }

    @Override //Hacer ultimo
    public BatoipopCategoria insertGenKey(BatoipopCategoria t) throws Exception {

     /*   pstInsertKey.setString(1, t.getDescripcionCategoria());
        pstInsertKey.setString(2, t.getName());

        int insertados = pstInsertKey.executeUpdate();
        if(insertados == 1){
            ResultSet resultSet = pstInsertKey.getGeneratedKeys();
            resultSet.next();
            t.setId(resultSet.getInt(1));
            return t;
        } return null;*/
        return null;
    }

    @Override
    public boolean update(BatoipopCategoria t) throws Exception {

      /*  pstUpdate.setString(1, t.getDescripcionCategoria());
        pstUpdate.setString(2, t.getName());
        pstUpdate.setInt(3, t.getId());

        int actualizados = pstUpdate.executeUpdate();
        return (actualizados == 1);*/

        URL url ;
        HttpURLConnection con;
        Gson gson;
        try {
            gson=new Gson();
            String empleadoJson=gson.toJson(t,BatoipopCategoria.class);
            url=new URL(HTTP);
            con=(HttpURLConnection) url.openConnection();
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");
            OutputStream os = con.getOutputStream();
            os.write(empleadoJson.getBytes());
            os.flush();

            if (con.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            }
            con.disconnect();



        } catch (IOException e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean delete(int id) throws Exception {
       /* pstDelete.setInt(1, id);
        int borrados = pstDelete.executeUpdate();
        return (borrados == 1);*/
        URL url;
        HttpURLConnection con;

        try {
            url = new URL(HTTP+id );
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("DELETE");
            con.setRequestProperty("Accept", "application/json");

            if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            }

            con.disconnect();

        } catch (IOException e) {
            return  false;
        }

        return  true;
    }

    @Override
    public boolean delete(BatoipopCategoria t) throws Exception {
        return this.delete(t.getId());
    }

    @Override
    public int size() throws Exception {
     /*   ResultSet rs = pstCount.executeQuery();
        rs.next();
        return rs.getInt(1);*/
        return 0;
    }

    @Override
    public boolean exists(int id) throws Exception {
        return findByPK(id) != null;

    }

    @Override  // IGNORANDO A MUERTE
    public List<BatoipopCategoria> findByExample(BatoipopCategoria t) throws Exception {
        return null;
    }
}
