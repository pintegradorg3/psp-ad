package dao;

import com.google.gson.Gson;
import pojos.BatoipopEmpleados;
import pojos.BatoipopFoto;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FotoDAO implements GenericDao.GenericDAO<BatoipopFoto>{
    final static String HTTP = "http://localhost:8080/foto/";
    @Override
    public BatoipopFoto findByPK(int id) throws Exception {
        String lin,salida="";
        BatoipopFoto empleado = null;
        Gson gson;
        URL url;
        HttpURLConnection con;
        try {
            url=new URL(HTTP+id);
            con=(HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Accept","application/json");

            if (con.getResponseCode()!=HttpURLConnection.HTTP_OK){
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            }else {
                BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
                while ((lin=br.readLine())!=null){
                    salida=salida.concat(lin);
                }
                gson=new Gson();
                empleado=gson.fromJson(salida,BatoipopFoto.class);
                con.disconnect();

            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        return empleado;

    }

    @Override
    public List<BatoipopFoto> findAll() throws Exception {
        String lin,salida="";
        List<BatoipopFoto> listaEmpleados=null;
        BatoipopFoto[] empleados;
        Gson gson;
        URL url;
        HttpURLConnection con;
        try {
            url=new URL(HTTP);
            con=(HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Accept","application/json");
            if (con.getResponseCode()!=HttpURLConnection.HTTP_OK){
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            }else {
                BufferedReader br =new BufferedReader(new InputStreamReader(con.getInputStream()));
                while ((lin=br.readLine())!=null){
                    salida=salida.concat(lin);
                }
                gson=new Gson();
                empleados=gson.fromJson(salida,BatoipopFoto[].class);
                listaEmpleados=new ArrayList<BatoipopFoto>();
                listaEmpleados= Arrays.asList(empleados);
                con.disconnect();

            }

        } catch (IOException | RuntimeException e) {
            e.printStackTrace();
        }
        return listaEmpleados;
    }

    @Override
    public boolean insert(BatoipopFoto t) throws Exception {
        URL url ;
        HttpURLConnection con;
        Gson gson;
        try {
            gson=new Gson();
            BatoipopFoto batoipopEmpleados=new BatoipopFoto(t.getNombre());
            String empleado=gson.toJson(batoipopEmpleados,BatoipopFoto.class);
            url=new URL(HTTP);
            con=(HttpURLConnection) url.openConnection();
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");
            OutputStream os =con.getOutputStream();
            os.write(empleado.getBytes());
            os.flush();
            return true;


        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public BatoipopFoto insertGenKey(BatoipopFoto t) throws Exception {
        return null;
    }

    @Override
    public boolean update(BatoipopFoto t) throws Exception {
        URL url ;
        HttpURLConnection con;
        Gson gson;
        try {
            gson=new Gson();
            String empleadoJson=gson.toJson(t,BatoipopFoto.class);
            url=new URL(HTTP);
            con=(HttpURLConnection) url.openConnection();
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");
            OutputStream os = con.getOutputStream();
            os.write(empleadoJson.getBytes());
            os.flush();

            if (con.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            }
            con.disconnect();



        } catch (IOException e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean delete(int id) throws Exception {
        URL url;
        HttpURLConnection con;

        try {
            url = new URL(HTTP+id );
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("DELETE");
            con.setRequestProperty("Accept", "application/json");

            if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            }

            con.disconnect();

        } catch (IOException e) {
            return  false;
        }

        return  true;
    }

    @Override
    public boolean delete(BatoipopFoto t) throws Exception {
        return this.delete(t.getId());
    }

    @Override
    public int size() throws Exception {
        return 0;
    }

    @Override
    public boolean exists(int id) throws Exception {
        return false;
    }

    @Override
    public List<BatoipopFoto> findByExample(BatoipopFoto t) throws Exception {
        return null;
    }
}
