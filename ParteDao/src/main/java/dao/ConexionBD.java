package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

// Patrón singleton
public class ConexionBD {

    //private static final String JDBC_URL = "jdbc:postgresql://137.74.226.43:5432/ProyectoIntegradorG3?currentSchema=public";
    private static final String JDBC_URL = "jdbc:postgresql://localhost:5432/ProyectoIntegradorG3?currentSchema=public";
    private static Connection con = null;

    public static Connection getConexion() throws SQLException {
        if (con == null) {
            Properties pc = new Properties();
            pc.put("user", "odoo");
            pc.put("password", "HolaCaracola12");
            con = DriverManager.getConnection(JDBC_URL, pc);
        }
        return con;
    }

    public static void cerrar() throws SQLException {
        if (con != null) {
            con.close();
            con = null;
        }
    }
}