package dao;

import com.google.gson.Gson;
import pojos.BatoipopEmpleados;
import pojos.BatoipopUsuario;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EmpleadoDAO implements GenericDao.GenericDAO<BatoipopEmpleados> {
    final static String TABLA="batoipop_empleados";
    final static String PK = "id";
    final static String HTTP = "http://localhost:8080/empleados/";

   /* final String SQLSELECTALL = "SELECT * FROM " + TABLA;
    final String SQLSELECTCOUNT = "SELECT count(*) FROM " + TABLA;
    final String SQLSELECTPK = "SELECT * FROM " + TABLA + " WHERE " + PK + " = ?";
    final String SQLSELECTEXAMPLE = "SELECT * FROM " + TABLA + " where nombre like ? and direccion like ?";
    final String SQLINSERT = "INSERT INTO " + TABLA + " (nombre, direccion) VALUES (?, ?)";
    final String SQLUPDATE = "UPDATE " + TABLA + " SET nombre = ?, direccion = ? WHERE " + PK + " = ?";
    final String SQLDELETE = "DELETE FROM " + TABLA + " WHERE " + PK + " = ?";
*/

    @Override
    public BatoipopEmpleados findByPK(int id) throws Exception {
        String lin,salida="";
        BatoipopEmpleados empleado = null;
        Gson gson;
        URL url;
        HttpURLConnection con;
        try {
            url=new URL(HTTP+id);
            con=(HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Accept","application/json");

            if (con.getResponseCode()!=HttpURLConnection.HTTP_OK){
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            }else {
                BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
                while ((lin=br.readLine())!=null){
                    salida=salida.concat(lin);
                }
                gson=new Gson();
                empleado=gson.fromJson(salida,BatoipopEmpleados.class);
                con.disconnect();

            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        return empleado;


    }

    @Override
    public List<BatoipopEmpleados> findAll() throws Exception {
        String lin,salida="";
        List<BatoipopEmpleados> listaEmpleados=null;
        BatoipopEmpleados[] empleados;
        Gson gson;
        URL url;
        HttpURLConnection con;
        try {
            url=new URL(HTTP);
            con=(HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Accept","application/json");
            if (con.getResponseCode()!=HttpURLConnection.HTTP_OK){
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            }else {
                BufferedReader br =new BufferedReader(new InputStreamReader(con.getInputStream()));
                while ((lin=br.readLine())!=null){
                    salida=salida.concat(lin);
                }
                gson=new Gson();
                empleados=gson.fromJson(salida,BatoipopEmpleados[].class);
                listaEmpleados=new ArrayList<BatoipopEmpleados>();
                listaEmpleados= Arrays.asList(empleados);
                con.disconnect();

            }

        } catch (IOException | RuntimeException e) {
            e.printStackTrace();
        }
        return listaEmpleados;
    }

    @Override
    public boolean insert(BatoipopEmpleados t) throws Exception {
        URL url ;
        HttpURLConnection con;
        Gson gson=new Gson();
        String empleadoJson= gson.toJson(t,BatoipopEmpleados.class);

        try {
            url=new URL(HTTP);
            con = (HttpURLConnection) url.openConnection();
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");

            OutputStream os =con.getOutputStream();
            os.write(empleadoJson.getBytes());
            os.flush();
            if (con.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
                return false;
                //throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            }

        } catch (Exception e) {
            return false;
            //e.printStackTrace();
        }

        return true;
    }

    @Override//Hacer ultimo
    public BatoipopEmpleados insertGenKey(BatoipopEmpleados t) throws Exception {

        return null;
    }

    @Override
    public boolean update(BatoipopEmpleados t) throws Exception {
        URL url ;
        HttpURLConnection con;
        Gson gson;
        try {
        gson=new Gson();
        String empleadoJson=gson.toJson(t,BatoipopEmpleados.class);
        url=new URL(HTTP);
        con=(HttpURLConnection) url.openConnection();
        con.setDoOutput(true);
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json");
        OutputStream os = con.getOutputStream();
        os.write(empleadoJson.getBytes());
        os.flush();

            if (con.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            }
            con.disconnect();



        } catch (IOException e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean delete(int id) throws Exception {
        URL url;
        HttpURLConnection con;

        try {
            url = new URL(HTTP+id );
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("DELETE");
            con.setRequestProperty("Accept", "application/json");

            if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            }

            con.disconnect();

        } catch (IOException e) {
            return  false;
        }

        return  true;
    }

    @Override
    public boolean delete(BatoipopEmpleados t) throws Exception {
        return this.delete(t.getId());
    }

    @Override
    public int size() throws Exception {
        return findAll().size();
    }

    @Override
    public boolean exists(int id) throws Exception {
        if (findByPK(id)!=null){
            return true;
        }
        return false;
    }

    @Override//No hacer demometo
    public List<BatoipopEmpleados> findByExample(BatoipopEmpleados t) throws Exception {
        return null;
    }
}
