package dao;

import com.google.gson.Gson;
import pojos.BatoipopArticles;
import pojos.BatoipopEmpleados;
import pojos.BatoipopUsuario;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UsuarioDAO implements GenericDao.GenericDAO<BatoipopUsuario> {
/*
    final String SQLSELECTALL = "SELECT * FROM batoipop_usuario ";
    final String SQLSELECTPK = "SELECT * FROM batoipop_usuario WHERE id = ?";
    final String SQLSELECTBYEMAIL = "SELECT * FROM batoipop_usuario WHERE correo_usuario = ? and contrasenya_usuario = ?";
    final String SQLINSERT = "INSERT INTO batoipop_usuario (contrasenyaUsuario, telefonoUsuario, nombreUsuario, apellidosUsuario, correoUsuario, codigoPostal, direccionUsuario, poblacion, name) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
    final String SQLUPDATEINFOUSUARIO = "UPDATE batoipop_usuario SET telefonoUsuario = ?,nombreUsuario = ?,apellidosUsuario = ?, codigoPostal = ?, direccionUsuario, poblacion = ? WHERE id = ?";
    final String SQLUPDATECONTRASENYA = "UPDATE batoipop_usuario SET contrasenyaUsuario = ? WHERE id = ? or correoUsuario = ?";
    final String SQLDELETE = "DELETE FROM batoipop_usuario WHERE id = ?";
    final String SQLCOUNT = "SELECT COUNT(*) FROM batoipop_usuario";
*/

    final static String TABLA = "batoipop_articles";
    final static String PK = "id";
    final static String HTTP = "http://localhost:8080/usuarios/";

    /*final String SQLSELECTALL = "SELECT * FROM " + TABLA;
    final String SQLSELECTCOUNT = "SELECT count(*) FROM " + TABLA;
    final String SQLSELECTPK = "SELECT * FROM " + TABLA + " WHERE " + PK + " = ?";
    final String SQLSELECTEXAMPLE = "SELECT * FROM " + TABLA + " where nombre like ? and direccion like ?";
    final String SQLINSERT = "INSERT INTO " + TABLA + " (nombre, direccion) VALUES (?, ?)";
    final String SQLUPDATE = "UPDATE " + TABLA + " SET nombre = ?, direccion = ? WHERE " + PK + " = ?";
    final String SQLDELETE = "DELETE FROM " + TABLA + " WHERE " + PK + " = ?";


   // final String SQLSELECTEXAMPLE = "SELECT * FROM batoipop_usuario where name like ? and precio <= ? and grupo like (?)";
    final String SQLSELECTEXAMPLE2 = "SELECT * FROM batoipop_usuario where name like ? and grupo like (?)";

    private final PreparedStatement pstSelectPK;
    private final PreparedStatement pstInsertKey;
    private final PreparedStatement pstSelectAll;
   // private final PreparedStatement pstSelectGrupo;
    private final PreparedStatement pstInsert;
   // private final PreparedStatement pstUpdate;
    private final PreparedStatement pstDelete;
    // private final PreparedStatement pstCount;
    // private final PreparedStatement pstEmail;


    private final PreparedStatement pstSelectExample;
    private final PreparedStatement pstSelectExample2;

*/
    public UsuarioDAO() throws SQLException {
       /* Connection con = ConexionBD.getConexion();
        pstSelectPK = con.prepareStatement(SQLSELECTPK);
        pstSelectAll = con.prepareStatement(SQLSELECTALL);
        pstInsert = con.prepareStatement(SQLINSERT);
      //  pstUpdate = con.prepareStatement(SQLUPDATEINFOUSUARIO);
        pstDelete = con.prepareStatement(SQLDELETE);
      //  pstCount = con.prepareStatement(SQLCOUNT);
        // pstSelectGrupo = con.prepareStatement(SQLSELECTALLARTICLESUSUARIOPUBLICADO);
        pstSelectExample = con.prepareStatement(SQLSELECTEXAMPLE);
        pstInsertKey = con.prepareStatement(SQLINSERT,PreparedStatement.RETURN_GENERATED_KEYS);
        pstSelectExample2 = con.prepareStatement(SQLSELECTEXAMPLE2);
*/
     //   pstEmail = con.prepareStatement(SQLSELECTBYEMAIL);
    }

    public void cerrar() throws SQLException {
      /*  pstSelectPK.close();
        pstSelectAll.close();
        pstInsert.close();
       // pstUpdate.close();
        pstDelete.close();*/
    }
    /*
    String contrasenyaUsuario, String telefonoUsuario, String nombreUsuario,
			String apellidosUsuario, String correoUsuario, String codigoPostal, String direccionUsuario,
			String poblacion, String name
     */
    @Override
    public BatoipopUsuario findByPK(int id) throws Exception {
        /*
        BatoipopUsuario b = null;
        pstSelectPK.setInt(1, id);
        ResultSet rs = pstSelectPK.executeQuery();

        if(rs.next()){
            b = new BatoipopUsuario(id, rs.getString("contrasenyaUsuario"), rs.getString("telefonoUsuario"),
                    rs.getString("nombreUsuario"), rs.getString("apellidosUsuario"), rs.getString("correoUsuario"),
                    rs.getString("codigoPostal"), rs.getString("direccionUsuario"), rs.getString("poblacion"),
                    rs.getString("name"));
        }
        rs.close();
        return b;
       */
        String lin, salida = "";
        BatoipopUsuario cliente = null;
        Gson gson;
        URL url;
        HttpURLConnection con;

        try {
            url = new URL(HTTP  + id);
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Accept", "application/json");

            if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
                while ((lin = br.readLine()) != null) {
                    salida = salida.concat(lin);
                }
                gson = new Gson();
                cliente = gson.fromJson(salida, BatoipopUsuario.class);

                con.disconnect();
                return cliente;
            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        return null;
    }

    @Override
    public List<BatoipopUsuario> findAll() throws Exception {
        String lin,salida="";
        List<BatoipopUsuario> batoipopUsuario=null;
        BatoipopUsuario[] usuarios;
        Gson gson;
        URL url;
        HttpURLConnection con;
        try {
            url=new URL(HTTP);
            con=(HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Accept","application/json");
            if (con.getResponseCode()!=HttpURLConnection.HTTP_OK){
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            }else {
                BufferedReader br =new BufferedReader(new InputStreamReader(con.getInputStream()));
                while ((lin=br.readLine())!=null){
                    salida=salida.concat(lin);
                }
                gson=new Gson();
                usuarios=gson.fromJson(salida,BatoipopUsuario[].class);
                batoipopUsuario=new ArrayList<BatoipopUsuario>();
                batoipopUsuario= Arrays.asList(usuarios);
                con.disconnect();

            }

        } catch (IOException | RuntimeException e) {
            e.printStackTrace();
        }
        return batoipopUsuario;
       /* List<BatoipopUsuario> batoipopArticles = new ArrayList<BatoipopUsuario>();
        ResultSet rs = pstSelectAll.executeQuery();


        while (rs.next()) {
            batoipopArticles.add(new BatoipopUsuario(rs.getInt("id"),rs.getString("contrasenyaUsuario"), rs.getString("telefonoUsuario"),
                    rs.getString("nombreUsuario"), rs.getString("apellidosUsuario"), rs.getString("correoUsuario"),
                    rs.getString("codigoPostal"), rs.getString("direccionUsuario"), rs.getString("poblacion"),
                    rs.getString("name")));
        }
        rs.close();
        return batoipopArticles;*/


    }

    @Override
    public boolean insert(BatoipopUsuario t) throws Exception {
        URL url ;
        HttpURLConnection con;
        Gson gson=new Gson();
        String empleadoJson= gson.toJson(t,BatoipopUsuario.class);

        try {
            url=new URL(HTTP);
            con = (HttpURLConnection) url.openConnection();
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");

            OutputStream os =con.getOutputStream();
            os.write(empleadoJson.getBytes());
            os.flush();
            if (con.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
                return false;
                //throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            }




        } catch (Exception e) {
            return false;
            //e.printStackTrace();
        }

        return true;
    }

    @Override //Hacer al final
    public BatoipopUsuario insertGenKey(BatoipopUsuario t) throws Exception {
       /* pstInsertKey.setString(1, t.getContrasenyaUsuario());
        pstInsertKey.setString(2, t.getTelefonoUsuario());
        pstInsertKey.setString(3, t.getNombreUsuario());
        pstInsertKey.setString(4, t.getApellidosUsuario());
        pstInsertKey.setString(5, t.getCorreoUsuario());
        pstInsertKey.setString(6, t.getCodigoPostal());
        pstInsertKey.setString(7, t.getDireccionUsuario());
        pstInsertKey.setString(8, t.getPoblacion());
        pstInsertKey.setString(9, t.getName());

        int insertados = pstInsertKey.executeUpdate();
        if(insertados == 1){
            ResultSet resultSet = pstInsertKey.getGeneratedKeys();
            resultSet.next();
            t.setId(resultSet.getInt(1));
            return t;*/
        //}
        return null;
    }
    // telefonoUsuario = ?,nombreUsuario = ?,apellidosUsuario = ?, codigoPostal = ?, direccionUsuario, poblacion = ? WHERE id = ?";

    @Override
    public boolean update(BatoipopUsuario t) throws Exception {
        URL url ;
        HttpURLConnection con;
        Gson gson;
        try {
            gson=new Gson();
            String usuariosJson=gson.toJson(t,BatoipopUsuario.class);
            url=new URL(HTTP);
            con = (HttpURLConnection) url.openConnection();
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");

            OutputStream os = con.getOutputStream();
            os.write(usuariosJson.getBytes());
            os.flush();


            if (con.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            }
            con.disconnect();

        } catch (IOException e) {
            return false;
        }
        return true;
        /*
        pstUpdate.setString(1, t.getTelefonoUsuario());
        pstUpdate.setString(2, t.getNombreUsuario());
        pstUpdate.setString(3, t.getApellidosUsuario());
        pstUpdate.setString(4, t.getCodigoPostal());
        pstUpdate.setString(5, t.getDireccionUsuario());
        pstUpdate.setString(6, t.getPoblacion());
        pstUpdate.setInt(7, t.getId());

        int actualizados = pstUpdate.executeUpdate();
        return (actualizados == 1);
         */
    }

    @Override
    public boolean delete(int id) throws Exception {
        URL url;
        HttpURLConnection con;

        try {
            url = new URL(HTTP+id );
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("DELETE");
            con.setRequestProperty("Accept", "application/json");

            if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            }

            con.disconnect();

        } catch (IOException e) {
            return  false;
        }

        return  true;
    }

    @Override
    public boolean delete(BatoipopUsuario t) throws Exception {
        return this.delete(t.getId());
    }

    @Override
    public int size() throws Exception {
       // ResultSet rs = pstCount.executeQuery();
       // rs.next();
       // return rs.getInt(1);
        return 0;
    }

    @Override
    public boolean exists(int id) throws Exception {
        return findByPK(id) != null;
    }

    @Override
    public List<BatoipopUsuario> findByExample(BatoipopUsuario t) throws Exception {
        return null;
    }
}
