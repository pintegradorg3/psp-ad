package dao;

import com.google.gson.Gson;
import pojos.BatoipopArticles;
import pojos.BatoipopCategoria;
import pojos.BatoipopEmpleados;
import pojos.BatoipopUsuario;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArticuloDao implements GenericDao.GenericDAO<BatoipopArticles> {


   /*
    final String SQLSELECTALL = "SELECT * FROM batoipop_articles ";
    final String SQLSELECTPK = "SELECT * FROM batoipop_articles WHERE id = ?";
    final String SQLSELECTALLARTICLESUSUARIOPUBLICADO = "SELECT * FROM batoipop_articles WHERE batoipopUsuarioByPublicarArticulo = ?";
    final String SQLINSERT = "INSERT INTO batoipop_articles (name, batoipopCategoria, batoipopUsuarioByPublicarArticulo, precioArticulo, descripcionArticulo, disponible) VALUES (?, ?, ?, ?, ?, ?)";
    final String SQLUPDATE = "UPDATE batoipop_articles SET name = ?,batoipopCategoria = ?,descripcionArticulo = ?, disponible = ?, precioArticulo = ? WHERE id = ?";
    final String SQLDELETE = "DELETE FROM batoipop_articles WHERE id = ?";
    final String SQLCOUNT = "SELECT COUNT(*) FROM batoipop_articles";
*/
    final static String TABLA = "batoipop_articles";
    final static String PK = "id";
    final static String HTTP = "http://localhost:8080/article/";

   /* final String SQLSELECTALL = "SELECT * FROM " + TABLA;
    final String SQLSELECTCOUNT = "SELECT count(*) FROM " + TABLA;
    final String SQLSELECTPK = "SELECT * FROM " + TABLA + " WHERE " + PK + " = ?";
    final String SQLSELECTEXAMPLE = "SELECT * FROM " + TABLA + " where nombre like ? and direccion like ?";
    final String SQLINSERT = "INSERT INTO " + TABLA + " (nombre, direccion) VALUES (?, ?)";
    final String SQLUPDATE = "UPDATE " + TABLA + " SET nombre = ?, direccion = ? WHERE " + PK + " = ?";
    final String SQLDELETE = "DELETE FROM " + TABLA + " WHERE " + PK + " = ?";



 //   final String SQLSELECTEXAMPLE = "SELECT * FROM batoipop_articles where name like ? and precio <= ? and grupo like (?)";
    final String SQLSELECTEXAMPLE2 = "SELECT * FROM batoipop_articles where name like ? and grupo like (?)";

    private final PreparedStatement pstSelectPK;
    private final PreparedStatement pstInsertKey;
    private final PreparedStatement pstSelectAll;
//    private final PreparedStatement pstSelectGrupo;
    private final PreparedStatement pstInsert;
    private final PreparedStatement pstUpdate;
    private final PreparedStatement pstDelete;
 //   private final PreparedStatement pstCount;


    private final PreparedStatement pstSelectExample;
    private final PreparedStatement pstSelectExample2;
*/
    public ArticuloDao() throws SQLException {
       /* Connection con = ConexionBD.getConexion();
        pstSelectPK = con.prepareStatement(SQLSELECTPK);
        pstSelectAll = con.prepareStatement(SQLSELECTALL);
        pstInsert = con.prepareStatement(SQLINSERT);
        pstUpdate = con.prepareStatement(SQLUPDATE);
        pstDelete = con.prepareStatement(SQLDELETE);
    //    pstCount = con.prepareStatement(SQLCOUNT);
    //    pstSelectGrupo = con.prepareStatement(SQLSELECTALLARTICLESUSUARIOPUBLICADO);
        pstSelectExample = con.prepareStatement(SQLSELECTEXAMPLE);
        pstInsertKey = con.prepareStatement(SQLINSERT,PreparedStatement.RETURN_GENERATED_KEYS);
        pstSelectExample2 = con.prepareStatement(SQLSELECTEXAMPLE2);*/
    }

    public void cerrar() throws SQLException {
       /* pstSelectPK.close();
        pstSelectAll.close();
        pstInsert.close();
        pstUpdate.close();
        pstDelete.close();*/
    }

    @Override // OKI
    public BatoipopArticles findByPK(int id) throws Exception {
        /*
        BatoipopArticles b = null;
        pstSelectPK.setInt(1, id);
        ResultSet rs = pstSelectPK.executeQuery();

        CategoriaDAO categoriaDAO = new CategoriaDAO();
        UsuarioDAO usuarioDAO = new UsuarioDAO();

        if(rs.next()){
            b = new BatoipopArticles(id, categoriaDAO.findByPK(rs.getInt("batoipopCategoria")), usuarioDAO.findByPK(rs.getInt("batoipopUsuarioByPublicarArticulo")),
                    rs.getString("descripcionArticulo"), rs.getDouble("precioArticulo"), rs.getBoolean("disponible"), rs.getString("name"));
        }
        rs.close();
        return b;*/

        String lin, salida = "";
        BatoipopArticles cliente = null;
        Gson gson;
        URL url;
        HttpURLConnection con;

        try {
            url = new URL(HTTP + id);
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Accept", "application/json");

            if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
                while ((lin = br.readLine()) != null) {
                    salida = salida.concat(lin);
                }
                gson = new Gson();
                cliente = gson.fromJson(salida, BatoipopArticles.class);

                con.disconnect();
                return cliente;
            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        return null;

    }

    @Override // OKI
    public List<BatoipopArticles> findAll() throws Exception {
       /* List<BatoipopArticles> batoipopArticles = new ArrayList<BatoipopArticles>();
        ResultSet rs = pstSelectAll.executeQuery();

        CategoriaDAO categoriaDAO = new CategoriaDAO();
        UsuarioDAO usuarioDAO = new UsuarioDAO();

        while (rs.next()) {
            batoipopArticles.add(new BatoipopArticles(rs.getInt("id"),  categoriaDAO.findByPK(rs.getInt("batoipopCategoria")), usuarioDAO.findByPK(rs.getInt("batoipopUsuarioByPublicarArticulo")),
                    rs.getString("descripcionArticulo"), rs.getDouble("precioArticulo"), rs.getBoolean("disponible"), rs.getString("name")));
        }
        rs.close();
        return batoipopArticles;*/
        String lin,salida="";
        List<BatoipopArticles> listaEmpleados=null;
        BatoipopArticles[] articulos;
        Gson gson;
        URL url;
        HttpURLConnection con;
        try {
            url=new URL(HTTP);
            con=(HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Accept","application/json");

            if (con.getResponseCode()!=HttpURLConnection.HTTP_OK){
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            }else {
                BufferedReader br =new BufferedReader(new InputStreamReader(con.getInputStream()));
                while ((lin=br.readLine())!=null){
                    salida=salida.concat(lin);
                }
                gson=new Gson();
                System.out.println(salida);
                articulos=gson.fromJson(salida,BatoipopArticles[].class);
                listaEmpleados= new ArrayList<>(Arrays.asList(articulos));
                con.disconnect();

            }

        } catch (IOException | RuntimeException e) {
            e.printStackTrace();
        }
        return listaEmpleados;

    }

    @Override // OKI
    public boolean insert(BatoipopArticles t) throws Exception {
        URL url ;
        HttpURLConnection con;
        Gson gson=new Gson();
        String articulo= gson.toJson(t,BatoipopArticles.class);
        System.out.println(articulo);

        try {
            url=new URL(HTTP);
            con = (HttpURLConnection) url.openConnection();
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");

            OutputStream os =con.getOutputStream();
            os.write(articulo.getBytes());
            os.flush();
            if (con.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
                return false;
                //throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            }

        } catch (Exception e) {
            return false;
            //e.printStackTrace();
        }

        return true;

    }

    @Override // OKI
    public BatoipopArticles insertGenKey(BatoipopArticles t) throws Exception {
       CategoriaDAO categoriaDAO = new CategoriaDAO();
        UsuarioDAO usuarioDAO = new UsuarioDAO();

        BatoipopCategoria g = categoriaDAO.findByPK(t.getBatoipopCategoria().getId());
        BatoipopUsuario u = usuarioDAO.findByPK(t.getBatoipopUsuarioByPublicarArticulo().getId());

        //pstInsertKey.setInt(1, g.getId());
        //pstInsertKey.setInt(2, u.getId());
        //pstInsertKey.setDouble(3, t.getPrecioArticulo());
        //pstInsertKey.setString(4, t.getDescripcionArticulo());
        //pstInsertKey.setBoolean(5, t.getDisponible());

        //int insertados = pstInsertKey.executeUpdate();
        //if(insertados == 1){
         //   ResultSet resultSet = pstInsertKey.getGeneratedKeys();
          //  resultSet.next();
            //t.setId(resultSet.getInt(1));
            //return t;
       // }
        return null;

    }

    @Override // OKI
    public boolean update(BatoipopArticles t) throws Exception {
        /*CategoriaDAO categoriaDAO = new CategoriaDAO();
        BatoipopCategoria g = categoriaDAO.findByPK(t.getBatoipopCategoria().getId());

        pstUpdate.setString(1, t.getName());
        pstUpdate.setInt(2, g.getId());
        pstUpdate.setString(3, t.getDescripcionArticulo());
        pstUpdate.setBoolean(4, t.getDisponible());
        pstUpdate.setDouble(5, t.getPrecioArticulo());
        pstUpdate.setInt(6, t.getId());

        int actualizados = pstUpdate.executeUpdate();
        return (actualizados == 1);*/
        URL url ;
        HttpURLConnection con;
        Gson gson;
        try {
            gson=new Gson();
            String empleadoJson=gson.toJson(t,BatoipopArticles.class);
            url=new URL(HTTP);
            con=(HttpURLConnection) url.openConnection();
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");
            OutputStream os = con.getOutputStream();
            os.write(empleadoJson.getBytes());
            os.flush();

            if (con.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            }
            con.disconnect();



        } catch (IOException e) {
            return false;
        }
        return true;

    }

    @Override // OKI
    public boolean delete(int id) throws Exception {
        URL url;
        HttpURLConnection con;

        try {
            url = new URL(HTTP+id );
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("DELETE");
            con.setRequestProperty("Accept", "application/json");

            if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            }

            con.disconnect();

        } catch (IOException e) {
            return  false;
        }

        return  true;
    }

    @Override
    public boolean delete(BatoipopArticles t) throws Exception {
        return this.delete(t.getId());
    }

    @Override
    public int size() throws Exception {
      //  ResultSet rs = pstCount.executeQuery();
      //  rs.next();
      //  return rs.getInt(1);
        return 0;
    }

    @Override
    public boolean exists(int id) throws Exception {
        return findByPK(id) != null;
    }

    @Override // IGNORANDO A MUERTE
    public List<BatoipopArticles> findByExample(BatoipopArticles t) throws Exception {
        return null;
    }
}
