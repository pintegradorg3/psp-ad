package pojos;
// Generated 20 feb 2022 13:22:09 by Hibernate Tools 4.3.5.Final

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * BatoipopMensaje generated by hbm2java
 */
@Entity
@Table(name = "batoipop_mensaje", schema = "public")
public class BatoipopMensaje implements java.io.Serializable {

	private int id;
	private BatoipopUsuario batoipopUsuario;
	private Integer createUid;
	private Date createDate;
	private Integer writeUid;
	private Date writeDate;
	private String name;
	private String mensaje;
	private BatoipopDenunciaMensaje batoipopDenunciaMensaje;

	public BatoipopMensaje() {
	}

	public BatoipopMensaje(int id, String mensaje) {
		this.id = id;
		this.mensaje = mensaje;
	}

	public BatoipopMensaje(int id, BatoipopUsuario batoipopUsuario, Integer createUid, Date createDate,
			Integer writeUid, Date writeDate, String name, String mensaje,
			BatoipopDenunciaMensaje batoipopDenunciaMensaje) {
		this.id = id;
		this.batoipopUsuario = batoipopUsuario;
		this.createUid = createUid;
		this.createDate = createDate;
		this.writeUid = writeUid;
		this.writeDate = writeDate;
		this.name = name;
		this.mensaje = mensaje;
		this.batoipopDenunciaMensaje = batoipopDenunciaMensaje;
	}

	@Id

	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_usuario_enviado")
	public BatoipopUsuario getBatoipopUsuario() {
		return this.batoipopUsuario;
	}

	public void setBatoipopUsuario(BatoipopUsuario batoipopUsuario) {
		this.batoipopUsuario = batoipopUsuario;
	}

	@Column(name = "create_uid")
	public Integer getCreateUid() {
		return this.createUid;
	}

	public void setCreateUid(Integer createUid) {
		this.createUid = createUid;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", length = 29)
	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Column(name = "write_uid")
	public Integer getWriteUid() {
		return this.writeUid;
	}

	public void setWriteUid(Integer writeUid) {
		this.writeUid = writeUid;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "write_date", length = 29)
	public Date getWriteDate() {
		return this.writeDate;
	}

	public void setWriteDate(Date writeDate) {
		this.writeDate = writeDate;
	}

	@Column(name = "name")
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "mensaje", nullable = false, length = 100)
	public String getMensaje() {
		return this.mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	@OneToOne(fetch = FetchType.LAZY, mappedBy = "batoipopMensaje")
	public BatoipopDenunciaMensaje getBatoipopDenunciaMensaje() {
		return this.batoipopDenunciaMensaje;
	}

	public void setBatoipopDenunciaMensaje(BatoipopDenunciaMensaje batoipopDenunciaMensaje) {
		this.batoipopDenunciaMensaje = batoipopDenunciaMensaje;
	}

}
