package com.batoipop.apiResBatoipop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class ApiResBatoipopApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiResBatoipopApplication.class, args);
	}
}