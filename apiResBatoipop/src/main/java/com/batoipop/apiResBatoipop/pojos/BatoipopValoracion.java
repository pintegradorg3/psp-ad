package com.batoipop.apiResBatoipop.pojos;
// Generated 22 feb 2022 18:32:43 by Hibernate Tools 4.3.5.Final

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 * BatoipopValoracion generated by hbm2java
 */
@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Entity
@Table(name = "batoipop_valoracion", schema = "public")
public class BatoipopValoracion implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private BatoipopUsuario batoipopUsuario;
	private String valoracion;
	@JsonIgnore
	private Integer createUid;
	@JsonIgnore
	private Date createDate;
	@JsonIgnore
	private Integer writeUid;
	@JsonIgnore
	private Date writeDate;

	public BatoipopValoracion() {
	}

	public BatoipopValoracion(int id, BatoipopUsuario batoipopUsuario) {
		this.id = id;
		this.batoipopUsuario = batoipopUsuario;
	}

	public BatoipopValoracion(int id, BatoipopUsuario batoipopUsuario, String valoracion, Integer createUid,
			Date createDate, Integer writeUid, Date writeDate) {
		this.id = id;
		this.batoipopUsuario = batoipopUsuario;
		this.valoracion = valoracion;
		this.createUid = createUid;
		this.createDate = createDate;
		this.writeUid = writeUid;
		this.writeDate = writeDate;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario")
	public BatoipopUsuario getBatoipopUsuario() {
		return this.batoipopUsuario;
	}

	public void setBatoipopUsuario(BatoipopUsuario batoipopUsuario) {
		this.batoipopUsuario = batoipopUsuario;
	}

	@Column(name = "valoracion")
	public String getValoracion() {
		return this.valoracion;
	}

	public void setValoracion(String valoracion) {
		this.valoracion = valoracion;
	}

	@Column(name = "create_uid")
	public Integer getCreateUid() {
		return this.createUid;
	}

	public void setCreateUid(Integer createUid) {
		this.createUid = createUid;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", length = 29)
	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Column(name = "write_uid")
	public Integer getWriteUid() {
		return this.writeUid;
	}

	public void setWriteUid(Integer writeUid) {
		this.writeUid = writeUid;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "write_date", length = 29)
	public Date getWriteDate() {
		return this.writeDate;
	}

	public void setWriteDate(Date writeDate) {
		this.writeDate = writeDate;
	}

}
