package com.batoipop.apiResBatoipop.pojos;


// Generated 22 feb 2022 18:32:43 by Hibernate Tools 4.3.5.Final

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;



/**
 * BatoipopUsuario generated by hbm2java
 */
@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Entity
@Table(name = "batoipop_usuario", schema = "public")
public class BatoipopUsuario implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String name;
	private String contrasenyaUsuario;
	private String telefonoUsuario;
	private String nombreUsuario;
	private String apellidosUsuario;
	private String correoUsuario;
	private String codigoPostal;
	private String direccionUsuario;
	private String poblacion;
	
	@JsonIgnore
	private Integer createUid;
	@JsonIgnore
	private Date createDate;
	@JsonIgnore
	private Integer writeUid;
	@JsonIgnore
	private Date writeDate;
	
	@JsonIgnore
	private Set<BatoipopArticles> batoipopArticlesForUsuarioCompraArticulo = new HashSet<BatoipopArticles>(0);
	@JsonIgnore
	private Set<BatoipopMensaje> batoipopMensajesForIdUsuarioEnviado = new HashSet<BatoipopMensaje>(0);
	@JsonIgnore
	private Set<BatoipopValoracion> batoipopValoraciones = new HashSet<BatoipopValoracion>(0);
	@JsonIgnore
	private Set<BatoipopArticles> batoipopArticlesForUsuarioPublicaArticulo = new HashSet<BatoipopArticles>(0);
	@JsonIgnore
	private Set<BatoipopMensaje> batoipopMensajesForIdUsuarioRecibido = new HashSet<BatoipopMensaje>(0);

	public BatoipopUsuario() {
	}

	public BatoipopUsuario(int id, String name, String contrasenyaUsuario, String telefonoUsuario, String nombreUsuario,
			String apellidosUsuario, String correoUsuario, String codigoPostal, String direccionUsuario) {
		this.id = id;
		this.name = name;
		this.contrasenyaUsuario = contrasenyaUsuario;
		this.telefonoUsuario = telefonoUsuario;
		this.nombreUsuario = nombreUsuario;
		this.apellidosUsuario = apellidosUsuario;
		this.correoUsuario = correoUsuario;
		this.codigoPostal = codigoPostal;
		this.direccionUsuario = direccionUsuario;
	}

	public BatoipopUsuario(int id, String name, String contrasenyaUsuario, String telefonoUsuario, String nombreUsuario,
			String apellidosUsuario, String correoUsuario, String codigoPostal, String direccionUsuario,
			String poblacion, Set<BatoipopArticles> batoipopArticlesForUsuarioCompraArticulo, Set<BatoipopMensaje> batoipopMensajesForIdUsuarioEnviado,
			Set<BatoipopValoracion> batoipopValoracions, Set<BatoipopArticles> batoipopArticlesForUsuarioPublicaArticulo,
			Set<BatoipopMensaje> batoipopMensajesForIdUsuarioRecibido) {
		this.id = id;
		this.name = name;
		this.contrasenyaUsuario = contrasenyaUsuario;
		this.telefonoUsuario = telefonoUsuario;
		this.nombreUsuario = nombreUsuario;
		this.apellidosUsuario = apellidosUsuario;
		this.correoUsuario = correoUsuario;
		this.codigoPostal = codigoPostal;
		this.direccionUsuario = direccionUsuario;
		this.poblacion = poblacion;
		this.batoipopArticlesForUsuarioCompraArticulo = batoipopArticlesForUsuarioCompraArticulo;
		this.batoipopMensajesForIdUsuarioEnviado = batoipopMensajesForIdUsuarioEnviado;
		this.batoipopValoraciones = batoipopValoracions;
		this.batoipopArticlesForUsuarioPublicaArticulo = batoipopArticlesForUsuarioPublicaArticulo;
		this.batoipopMensajesForIdUsuarioRecibido = batoipopMensajesForIdUsuarioRecibido;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "name", nullable = false)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "contrasenya_usuario", nullable = false)
	public String getContrasenyaUsuario() {
		return this.contrasenyaUsuario;
	}

	public void setContrasenyaUsuario(String contrasenyaUsuario) {
		this.contrasenyaUsuario = contrasenyaUsuario;
	}

	@Column(name = "telefono_usuario", nullable = false, length = 9)
	public String getTelefonoUsuario() {
		return this.telefonoUsuario;
	}

	public void setTelefonoUsuario(String telefonoUsuario) {
		this.telefonoUsuario = telefonoUsuario;
	}

	@Column(name = "nombre_usuario", nullable = false)
	public String getNombreUsuario() {
		return this.nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	@Column(name = "apellidos_usuario", nullable = false)
	public String getApellidosUsuario() {
		return this.apellidosUsuario;
	}

	public void setApellidosUsuario(String apellidosUsuario) {
		this.apellidosUsuario = apellidosUsuario;
	}

	@Column(name = "correo_usuario", nullable = false)
	public String getCorreoUsuario() {
		return this.correoUsuario;
	}

	public void setCorreoUsuario(String correoUsuario) {
		this.correoUsuario = correoUsuario;
	}

	@Column(name = "codigo_postal", nullable = false, length = 10)
	public String getCodigoPostal() {
		return this.codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	@Column(name = "direccion_usuario", nullable = false)
	public String getDireccionUsuario() {
		return this.direccionUsuario;
	}

	public void setDireccionUsuario(String direccionUsuario) {
		this.direccionUsuario = direccionUsuario;
	}

	@Column(name = "poblacion")
	public String getPoblacion() {
		return this.poblacion;
	}

	public void setPoblacion(String poblacion) {
		this.poblacion = poblacion;
	}

	@Column(name = "create_uid")
	public Integer getCreateUid() {
		return this.createUid;
	}

	public void setCreateUid(Integer createUid) {
		this.createUid = createUid;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", length = 29)
	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Column(name = "write_uid")
	public Integer getWriteUid() {
		return this.writeUid;
	}

	public void setWriteUid(Integer writeUid) {
		this.writeUid = writeUid;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "write_date", length = 29)
	public Date getWriteDate() {
		return this.writeDate;
	}

	public void setWriteDate(Date writeDate) {
		this.writeDate = writeDate;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "batoipopUsuarioByUsuarioCompraArticulo")
	public Set<BatoipopArticles> getBatoipopArticlesForUsuarioCompraArticulo() {
		return this.batoipopArticlesForUsuarioCompraArticulo;
	}

	public void setBatoipopArticlesForUsuarioCompraArticulo(Set<BatoipopArticles> batoipopArticlesForUsuarioCompraArticulo) {
		this.batoipopArticlesForUsuarioCompraArticulo = batoipopArticlesForUsuarioCompraArticulo;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "batoipopUsuarioByIdUsuarioEnviado")
	public Set<BatoipopMensaje> getBatoipopMensajesForIdUsuarioEnviado() {
		return this.batoipopMensajesForIdUsuarioEnviado;
	}

	public void setBatoipopMensajesForIdUsuarioEnviado(Set<BatoipopMensaje> batoipopMensajesForIdUsuarioEnviado) {
		this.batoipopMensajesForIdUsuarioEnviado = batoipopMensajesForIdUsuarioEnviado;
	}
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "batoipopUsuario")
	public Set<BatoipopValoracion> getBatoipopValoracions() {
		return this.batoipopValoraciones;
	}

	public void setBatoipopValoracions(Set<BatoipopValoracion> batoipopValoracions) {
		this.batoipopValoraciones = batoipopValoracions;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "batoipopUsuarioByUsuarioPublicaArticulo")
	public Set<BatoipopArticles> getBatoipopArticlesForUsuarioPublicaArticulo() {
		return this.batoipopArticlesForUsuarioPublicaArticulo;
	}

	public void setBatoipopArticlesForUsuarioPublicaArticulo(Set<BatoipopArticles> batoipopArticlesForUsuarioPublicaArticulo) {
		this.batoipopArticlesForUsuarioPublicaArticulo = batoipopArticlesForUsuarioPublicaArticulo;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "batoipopUsuarioByIdUsuarioRecibido")
	public Set<BatoipopMensaje> getBatoipopMensajesForIdUsuarioRecibido() {
		return this.batoipopMensajesForIdUsuarioRecibido;
	}

	public void setBatoipopMensajesForIdUsuarioRecibido(Set<BatoipopMensaje> batoipopMensajesForIdUsuarioRecibido) {
		this.batoipopMensajesForIdUsuarioRecibido = batoipopMensajesForIdUsuarioRecibido;
	}

}
