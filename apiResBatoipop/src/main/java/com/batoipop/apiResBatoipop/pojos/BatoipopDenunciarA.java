package com.batoipop.apiResBatoipop.pojos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "batoipop_denunciar_a", schema = "public")
public class BatoipopDenunciarA implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private BatoipopArticles batoipopArticles;
	private String description;

	public BatoipopDenunciarA() {
	}

	public BatoipopDenunciarA(int id, String description) {
		this.id = id;
		this.description = description;
	}

	public BatoipopDenunciarA(int id, BatoipopArticles batoipopArticles, String description) {
		this.id = id;
		this.batoipopArticles = batoipopArticles;
		this.description = description;
	}

	@Id

	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "articulo")
	@JsonBackReference
	public BatoipopArticles getBatoipopArticles() {
		return this.batoipopArticles;
	}

	public void setBatoipopArticles(BatoipopArticles batoipopArticles) {
		this.batoipopArticles = batoipopArticles;
	}

	@Column(name = "description", nullable = false)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
