package com.batoipop.apiResBatoipop.Controller;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.batoipop.apiResBatoipop.Service.EmpleadosService;
import com.batoipop.apiResBatoipop.pojos.BatoipopEmpleados;

@RestController
@RequestMapping("/empleados")
public class BatoipopEmpleadosController {
	
	@Autowired
	private EmpleadosService empleadosService;
	
    @GetMapping("")
    public List<BatoipopEmpleados> list() {
        return empleadosService.listAllEmpleados();
    }

    @GetMapping("/{id}")
    public ResponseEntity<BatoipopEmpleados> get(@PathVariable Integer id) {
        try {
        	BatoipopEmpleados cli = empleadosService.getEmpleado(id);
            return new ResponseEntity<BatoipopEmpleados>(cli, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<BatoipopEmpleados>(HttpStatus.NOT_FOUND);
        }
    }
    
    @GetMapping("/c={correoEmpleado}")
    public ResponseEntity<BatoipopEmpleados> getCorreo(@PathVariable String correoEmpleado) {
        try {
        	BatoipopEmpleados cli = empleadosService.getEmpleadoCorreo(correoEmpleado);
            return new ResponseEntity<BatoipopEmpleados>(cli, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<BatoipopEmpleados>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("")
    public ResponseEntity<?> add(@RequestBody BatoipopEmpleados cli) {
    	BatoipopEmpleados cliNuevo = empleadosService.saveUsuario(cli);
        return new ResponseEntity<>(cliNuevo.getId(), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody BatoipopEmpleados cli, @PathVariable Integer id) {
        try {
        	BatoipopEmpleados actualCli = empleadosService.getEmpleado(id);
            actualCli.setName(cli.getName());
            actualCli.setApellidosEmpleado(cli.getApellidosEmpleado());
            actualCli.setContrasenyaEmpleado(cli.getContrasenyaEmpleado());
            actualCli.setTelefonoEmpleado(cli.getTelefonoEmpleado());
            actualCli.setCorreoEmpleado(cli.getCorreoEmpleado());
            actualCli.setDireccionEmpleado(cli.getDireccionEmpleado());   
            actualCli.setPoblacion(cli.getPoblacion());

            empleadosService.saveUsuario(actualCli);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        try {
        	empleadosService.deleteEmpleado(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (EmptyResultDataAccessException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
