package com.batoipop.apiResBatoipop.Controller;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.batoipop.apiResBatoipop.Service.MensajeService;
import com.batoipop.apiResBatoipop.pojos.BatoipopMensaje;

@RestController
@RequestMapping("/mensaje")
public class BatoipopMensajeController {
	
	@Autowired
    MensajeService mensajeService;

    @GetMapping("")
    public List<BatoipopMensaje> list() {
        return mensajeService.listAllMensaje();
    }

    @GetMapping("/{id}")
    public ResponseEntity<BatoipopMensaje> get(@PathVariable Integer id) {
        try {
        	BatoipopMensaje cli = mensajeService.getMensaje(id);
            return new ResponseEntity<BatoipopMensaje>(cli, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<BatoipopMensaje>(HttpStatus.NOT_FOUND);
        }
    }
    
    @GetMapping("/ids={id_usuario_enviado}")
    public List<BatoipopMensaje> getMensajesEnviados(@PathVariable Integer idUsuario) {
    	return mensajeService.getMensajesEnviados(idUsuario); 
    }
    
    @GetMapping("/idr={id_usuario_recibido}")
    public List<BatoipopMensaje> getMensajesRecibidos(@PathVariable Integer idUsuario) {
    	return mensajeService.getMensajesRecibidos(idUsuario); 
    }
    
    @PostMapping("")
    public ResponseEntity<?> add(@RequestBody BatoipopMensaje cli) {
        BatoipopMensaje cliNuevo = mensajeService.saveMensaje(cli);
        return new ResponseEntity<>(cliNuevo.getId(), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody BatoipopMensaje msn, @PathVariable Integer id) {
        try {
        	BatoipopMensaje actualMensaje = mensajeService.getMensaje(id);
        	actualMensaje.setId(msn.getId());
        	actualMensaje.setBatoipopArticles(msn.getBatoipopArticles());
        	
        	actualMensaje.setBatoipopUsuarioByIdUsuarioEnviado(msn.getBatoipopUsuarioByIdUsuarioEnviado());
        	actualMensaje.setBatoipopUsuarioByIdUsuarioRecibido(msn.getBatoipopUsuarioByIdUsuarioRecibido());
        	
        	actualMensaje.setMensaje(msn.getMensaje());
        	actualMensaje.setName(msn.getName());
        	
            mensajeService.saveMensaje(actualMensaje);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        try {
        	mensajeService.deleteMensaje(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (EmptyResultDataAccessException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
