package com.batoipop.apiResBatoipop.Controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.batoipop.apiResBatoipop.Service.UsuarioService;
import com.batoipop.apiResBatoipop.pojos.BatoipopUsuario;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/usuarios")
public class BatoipopUsuarioController {
    
	@Autowired
    UsuarioService usuarioService;

    @GetMapping("")
    public List<BatoipopUsuario> list() {
        return usuarioService.listAllUsuarios();
    }

    @GetMapping("/{id}")
    public ResponseEntity<BatoipopUsuario> get(@PathVariable Integer id) {
        try {
            BatoipopUsuario cli = usuarioService.getUsuario(id);
            return new ResponseEntity<BatoipopUsuario>(cli, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<BatoipopUsuario>(HttpStatus.NOT_FOUND);
        }
    }
    
    @GetMapping("/p={poblacion}")
    public List<BatoipopUsuario> getPoblacion(@PathVariable String poblacion) {
    	return usuarioService.getUsuarioPoblacion(poblacion);
    }
    
    @GetMapping("/cp={codigoPostal}")
    public List<BatoipopUsuario> getCP(@PathVariable String codigoPostal) {
        return  usuarioService.getUsuarioCP(codigoPostal);
    }
    
    @GetMapping("/c={correoUsuario}")
    public ResponseEntity<BatoipopUsuario> getCorreo(@PathVariable String correoUsuario) {
        try {
            BatoipopUsuario cli = usuarioService.getUsuarioCorreo(correoUsuario);
            return new ResponseEntity<BatoipopUsuario>(cli, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<BatoipopUsuario>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "", produces = "application/json")
    public ResponseEntity<?> add(@RequestBody BatoipopUsuario cli) {
        BatoipopUsuario cliNuevo = usuarioService.saveUsuario(cli);
        return new ResponseEntity<>(cliNuevo.getId(), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody BatoipopUsuario cli, @PathVariable Integer id) {
        try {
            BatoipopUsuario actualCli = usuarioService.getUsuario(id);
            actualCli.setId(cli.getId());
            actualCli.setNombreUsuario(cli.getNombreUsuario());
            actualCli.setApellidosUsuario(cli.getApellidosUsuario());
            actualCli.setContrasenyaUsuario(cli.getContrasenyaUsuario());
            actualCli.setDireccionUsuario(cli.getDireccionUsuario());
            actualCli.setCodigoPostal(cli.getCodigoPostal());
            actualCli.setTelefonoUsuario(cli.getTelefonoUsuario());
            actualCli.setDireccionUsuario(cli.getDireccionUsuario());
            actualCli.setPoblacion(cli.getPoblacion());
            
            actualCli.setBatoipopArticlesForUsuarioCompraArticulo(cli.getBatoipopArticlesForUsuarioCompraArticulo());
            actualCli.setBatoipopMensajesForIdUsuarioEnviado(cli.getBatoipopMensajesForIdUsuarioEnviado());
            actualCli.setBatoipopValoracions(cli.getBatoipopValoracions());
            actualCli.setBatoipopArticlesForUsuarioPublicaArticulo(cli.getBatoipopArticlesForUsuarioPublicaArticulo());
            actualCli.setBatoipopMensajesForIdUsuarioRecibido(cli.getBatoipopMensajesForIdUsuarioRecibido());
            
            usuarioService.saveUsuario(actualCli);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        try {
            usuarioService.deleteUsuario(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (EmptyResultDataAccessException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
