package com.batoipop.apiResBatoipop.Controller;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.batoipop.apiResBatoipop.Service.ValoracionService;
import com.batoipop.apiResBatoipop.pojos.BatoipopValoracion;
@RestController
@RequestMapping("/valoracion")
public class BatoipopValoracionController {
	@Autowired
    ValoracionService valoracionService;

    @GetMapping("")
    public List<BatoipopValoracion> list() {
        return valoracionService.listAllValoraciones();
    }

    @GetMapping("/{id}")
    public ResponseEntity<BatoipopValoracion> get(@PathVariable Integer id) {
        try {
        	BatoipopValoracion val = valoracionService.getValoracion(id);
            return new ResponseEntity<BatoipopValoracion>(val, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<BatoipopValoracion>(HttpStatus.NOT_FOUND);
        }
    }
    
    
    @GetMapping("/us={usuario}")
    public List<BatoipopValoracion> getByUsuario(@PathVariable Integer id){
    	return valoracionService.listAllValoracionByUsuario(id);
    }
    
    
    @PostMapping("")
    public ResponseEntity<?> add(@RequestBody BatoipopValoracion val) {
    	BatoipopValoracion nuevaValoracion = valoracionService.saveValoracion(val);
        return new ResponseEntity<>(nuevaValoracion.getId(), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody BatoipopValoracion val, @PathVariable Integer id) {
        try {
        	BatoipopValoracion actualValoracion = valoracionService.getValoracion(id);
        	
        	actualValoracion.setId(val.getId());
        	actualValoracion.setBatoipopUsuario(val.getBatoipopUsuario());
        	actualValoracion.setValoracion(val.getValoracion());

            valoracionService.saveValoracion(actualValoracion);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        try {
        	valoracionService.deleteValoracion(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (EmptyResultDataAccessException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
