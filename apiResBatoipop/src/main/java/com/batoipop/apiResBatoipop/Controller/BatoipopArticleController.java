package com.batoipop.apiResBatoipop.Controller;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.batoipop.apiResBatoipop.Service.ArticlesService;
import com.batoipop.apiResBatoipop.pojos.BatoipopArticles;

@RestController
@RequestMapping("/article")
public class BatoipopArticleController {

	@Autowired
	private ArticlesService articleService;
	
    @GetMapping("")
    public List<BatoipopArticles> list() {
        return articleService.listAllArticles();
    }

    @GetMapping("/{id}")
    public ResponseEntity<BatoipopArticles> get(@PathVariable Integer id) {
        try {
        	BatoipopArticles art = articleService.getArticle(id);
            return new ResponseEntity<BatoipopArticles>(art, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<BatoipopArticles>(HttpStatus.NOT_FOUND);
        }
    }
    
    @GetMapping("/cat={categoria}")
    public List<BatoipopArticles> listByCategoria(@PathVariable Integer id) {
        return articleService.listAllArticlesByCategoria(id);
    }
    
    @GetMapping("/usu={usuario}")
    public List<BatoipopArticles> listByUsuario(@PathVariable Integer id) {
        return articleService.listAllArticlesByUsuario(id);
    }
    
    @GetMapping("/p={poblacion}")
    public List<BatoipopArticles> listByPoblacion(@PathVariable String poblacion) {
        return articleService.listAllArticlesByPoblacion(poblacion);
    }
    
    @GetMapping("/pmx={precio_articulo}")
    public List<BatoipopArticles> listByPrecioMax(@PathVariable double precio) {
        return articleService.listAllArticlesByPrecioMax(precio);
    }
    
    @GetMapping("/d={disponible}")
    public List<BatoipopArticles> listByDisponible() {
        return articleService.listAllArticlesDisponibles();
    }

    @PostMapping("")
    public ResponseEntity<?> add(@RequestBody BatoipopArticles art) {
    	BatoipopArticles artNuevo = articleService.saveArticle(art);
        return new ResponseEntity<>(artNuevo.getId(), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody BatoipopArticles art, @PathVariable Integer id) {
        try {
        	BatoipopArticles artActual = articleService.getArticle(id);
        	
        	artActual.setId(art.getId());
        	artActual.setBatoipopCategoria(art.getBatoipopCategoria());
        	
        	artActual.setBatoipopUsuarioByUsuarioCompraArticulo(art.getBatoipopUsuarioByUsuarioCompraArticulo());
        	artActual.setBatoipopUsuarioByUsuarioPublicaArticulo(art.getBatoipopUsuarioByUsuarioPublicaArticulo());
        	artActual.setName(art.getName());
        	
        	artActual.setDescripcionArticulo(art.getDescripcionArticulo());
        	artActual.setPrecioArticulo(art.getPrecioArticulo());
        	artActual.setDisponible(art.getDisponible());
        	
        	artActual.setBatoipopMensajes(art.getBatoipopMensajes());
        	artActual.setBatoipopDenunciarAs(art.getBatoipopDenunciarAs());
        	
        	articleService.saveArticle(artActual);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        try {
        	articleService.deleteArticle(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (EmptyResultDataAccessException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
