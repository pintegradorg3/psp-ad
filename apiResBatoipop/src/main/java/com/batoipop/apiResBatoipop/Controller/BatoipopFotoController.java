package com.batoipop.apiResBatoipop.Controller;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.batoipop.apiResBatoipop.Service.FotoService;
import com.batoipop.apiResBatoipop.pojos.BatoipopFoto;

@RestController
@RequestMapping("/foto")
public class BatoipopFotoController {
	
	@Autowired
	private FotoService fotoService;
	
    @GetMapping("")
    public List<BatoipopFoto> list() {
        return fotoService.listAllFotos();
    }

    @GetMapping("/{id}")
    public ResponseEntity<BatoipopFoto> get(@PathVariable Integer id) {
        try {
        	BatoipopFoto foto = fotoService.getFoto(id);
            return new ResponseEntity<BatoipopFoto>(foto, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<BatoipopFoto>(HttpStatus.NOT_FOUND);
        }
    }


    @PostMapping("")
    public ResponseEntity<?> add(@RequestBody BatoipopFoto foto) {
    	BatoipopFoto fotoNueva = fotoService.saveFoto(foto);
        return new ResponseEntity<>(fotoNueva.getId(), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody BatoipopFoto foto, @PathVariable Integer id) {
        try {
        	BatoipopFoto fotoActual = fotoService.getFoto(id);
            fotoActual.setId(foto.getId());
            fotoActual.setName(foto.getName());

            fotoService.saveFoto(fotoActual);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        try {
        	fotoService.deleteFoto(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (EmptyResultDataAccessException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
	

}
