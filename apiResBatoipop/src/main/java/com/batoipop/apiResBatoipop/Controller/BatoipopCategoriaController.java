package com.batoipop.apiResBatoipop.Controller;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.batoipop.apiResBatoipop.Service.CategoriaService;
import com.batoipop.apiResBatoipop.pojos.BatoipopCategoria;

@RestController
@RequestMapping("/categoria")
public class BatoipopCategoriaController {
	
	
	@Autowired
	private CategoriaService categoriaService;
	
    @GetMapping("")
    public List<BatoipopCategoria> list() {
        return categoriaService.listAllCategorias();
    }

    @GetMapping("/{id}")
    public ResponseEntity<BatoipopCategoria> get(@PathVariable Integer id) {
        try {
        	BatoipopCategoria cat = categoriaService.getCategoria(id);
            return new ResponseEntity<BatoipopCategoria>(cat, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<BatoipopCategoria>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("")
    public ResponseEntity<?> add(@RequestBody BatoipopCategoria cat) {
    	BatoipopCategoria catNueva = categoriaService.saveCategoria(cat);
        return new ResponseEntity<>(catNueva.getId(), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody BatoipopCategoria cat, @PathVariable Integer id) {
        try {
        	BatoipopCategoria catActual = categoriaService.getCategoria(id);
        	catActual.setId(cat.getId());
        	catActual.setName(cat.getName());
        	catActual.setDescripcionCategoria(cat.getDescripcionCategoria());
        	catActual.setBatoipopArticles(cat.getBatoipopArticles());
        	
            categoriaService.saveCategoria(catActual);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        try {
        	categoriaService.deleteCategoria(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (EmptyResultDataAccessException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
