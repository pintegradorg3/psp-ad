package com.batoipop.apiResBatoipop.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.batoipop.apiResBatoipop.Repository.FotoRepository;
import com.batoipop.apiResBatoipop.pojos.BatoipopFoto;

@Service
public class FotoService {
	 @Autowired
	    private FotoRepository fotoRepository;
	 
	 
	 public List<BatoipopFoto> listAllFotos() {
	        return fotoRepository.findAll();
	    }

	    public BatoipopFoto getFoto(Integer id) {
	        return fotoRepository.findById(id).get();
	    }

	    public BatoipopFoto saveFoto(BatoipopFoto foto) {
	        return fotoRepository.save(foto);
	    }

	    public void deleteFoto(Integer id) {
	    	fotoRepository.deleteById(id);
	    }
}
