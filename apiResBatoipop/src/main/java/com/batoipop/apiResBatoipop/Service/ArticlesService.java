package com.batoipop.apiResBatoipop.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.batoipop.apiResBatoipop.Repository.ArticlesRepository;
import com.batoipop.apiResBatoipop.pojos.BatoipopArticles;

@Service
public class ArticlesService {
	
	 @Autowired
		private ArticlesRepository articleRepository;
	  
	  
	  	public List<BatoipopArticles> listAllArticles() {
	        return articleRepository.findAll();
	    }

	    public BatoipopArticles getArticle(Integer id) {
	        return articleRepository.findById(id).get();
	    }

	    public BatoipopArticles saveArticle(BatoipopArticles art) {
	    	art = articleRepository.save(art);
	        return art;
	    }

	    public void deleteArticle(Integer id) {
	    	articleRepository.deleteById(id);
	    }
	    
	    
	  	public List<BatoipopArticles> listAllArticlesByCategoria(Integer id) {
	        return articleRepository.findAllByIdCategoria(id);
	    }
	  	
	  	public List<BatoipopArticles> listAllArticlesByUsuario(Integer id) {
	        return articleRepository.findAllByIdUsuario(id);
	    }
	  	
	  	public List<BatoipopArticles> listAllArticlesByPoblacion(String poblacion) {
	        return articleRepository.findAllPoblacion(poblacion);
	    }
	  	
	  	public List<BatoipopArticles> listAllArticlesByPrecioMax(double precio) {
	        return articleRepository.findAllByPrecioMAX(precio);
	    }
	  	
	  	public List<BatoipopArticles> listAllArticlesDisponibles() {
	        return articleRepository.findAllByDisponible();
	    }

}
