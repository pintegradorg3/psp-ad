package com.batoipop.apiResBatoipop.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.batoipop.apiResBatoipop.Repository.ValoracionRepository;
import com.batoipop.apiResBatoipop.pojos.BatoipopValoracion;

@Service
public class ValoracionService {
	@Autowired
	private ValoracionRepository valoracionRepository;
	
	 public List<BatoipopValoracion> listAllValoraciones() {
	        return valoracionRepository.findAll();
	    }

	    public BatoipopValoracion getValoracion(Integer id) {
	        return valoracionRepository.findById(id).get();
	    }
	   
	    public BatoipopValoracion saveValoracion(BatoipopValoracion cli) {
	        return valoracionRepository.save(cli);
	    }

	    public void deleteValoracion(Integer id) {
	    	valoracionRepository.deleteById(id);
	    }
	    
	    public List<BatoipopValoracion> listAllValoracionByUsuario(Integer id){
	    	return valoracionRepository.findAllByUser(id);
	    }

}
