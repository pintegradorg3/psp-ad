package com.batoipop.apiResBatoipop.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.batoipop.apiResBatoipop.Repository.MensajeRepository;
import com.batoipop.apiResBatoipop.pojos.BatoipopMensaje;
@Service
public class MensajeService {
	 @Autowired
	    private MensajeRepository mensajeRepository;

	    public List<BatoipopMensaje> listAllMensaje() {
	        return mensajeRepository.findAll();
	    }

	    public BatoipopMensaje getMensaje(Integer id) {
	        return mensajeRepository.findById(id).get();
	    }

	    public BatoipopMensaje saveMensaje(BatoipopMensaje cli) {
	        return mensajeRepository.save(cli);
	    }

	    public void deleteMensaje(Integer id) {
	        mensajeRepository.deleteById(id);
	    }
	    
	    public List<BatoipopMensaje> getMensajesEnviados(Integer id) {
	    	return mensajeRepository.findAllByIdUserEnviado(id);
	    }

	    public List<BatoipopMensaje> getMensajesRecibidos(Integer id) {
	    	return mensajeRepository.findAllByIdUserRecibido(id);
	    }
}
