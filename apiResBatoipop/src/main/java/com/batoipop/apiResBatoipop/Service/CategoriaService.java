package com.batoipop.apiResBatoipop.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.batoipop.apiResBatoipop.Repository.CategoriaRepository;
import com.batoipop.apiResBatoipop.pojos.BatoipopCategoria;
@Service
public class CategoriaService {
	
	  @Autowired
		private CategoriaRepository categoriaRepository;
	  
	  
	  	public List<BatoipopCategoria> listAllCategorias() {
	        return categoriaRepository.findAll();
	    }

	    public BatoipopCategoria getCategoria(Integer id) {
	        return categoriaRepository.findById(id).get();
	    }

	    public BatoipopCategoria saveCategoria(BatoipopCategoria cat) {
	        cat = categoriaRepository.save(cat);
	        return cat;
	    }

	    public void deleteCategoria(Integer id) {
	    	categoriaRepository.deleteById(id);
	    }


}
