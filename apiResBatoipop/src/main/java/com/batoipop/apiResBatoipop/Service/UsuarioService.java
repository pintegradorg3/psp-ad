package com.batoipop.apiResBatoipop.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.batoipop.apiResBatoipop.Repository.UsuarioRepository;
import com.batoipop.apiResBatoipop.pojos.BatoipopUsuario;


import java.util.List;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    public List<BatoipopUsuario> listAllUsuarios() {
        return usuarioRepository.findAll();
    }

    public BatoipopUsuario getUsuario(Integer id) {
        return usuarioRepository.findById(id).get();
    }
   
    public BatoipopUsuario getUsuarioCorreo(String email) {
        return usuarioRepository.findByCorreoUsuario(email);
    }

    public BatoipopUsuario saveUsuario(BatoipopUsuario cli) {
        return usuarioRepository.save(cli);
    }

    public void deleteUsuario(Integer id) {
        usuarioRepository.deleteById(id);
    }

	public List<BatoipopUsuario> getUsuarioCP(String codigoPostal) {
		return usuarioRepository.findByCP(codigoPostal);
	}
	
	public List<BatoipopUsuario> getUsuarioPoblacion(String poblacion) {
		return usuarioRepository.findByPoblacion(poblacion);
	}


}
