package com.batoipop.apiResBatoipop.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.batoipop.apiResBatoipop.Repository.EmpleadosRepository;
import com.batoipop.apiResBatoipop.pojos.BatoipopEmpleados;

@Service
public class EmpleadosService {
	
    @Autowired
	private EmpleadosRepository empleadosRepository;
	
	public List<BatoipopEmpleados> listAllEmpleados() {
        return empleadosRepository.findAll();
    }

    public BatoipopEmpleados getEmpleado(Integer id) {
        return empleadosRepository.findById(id).get();
    }
   
    public BatoipopEmpleados getEmpleadoCorreo(String email) {
        return empleadosRepository.findByCorreoEmpleado(email);
    }

    public BatoipopEmpleados saveUsuario(BatoipopEmpleados cli) {
        cli = empleadosRepository.save(cli);
        return cli;
    }

    public void deleteEmpleado(Integer id) {
    	empleadosRepository.deleteById(id);
    }

}
