package com.batoipop.apiResBatoipop.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.batoipop.apiResBatoipop.pojos.BatoipopMensaje;

public interface MensajeRepository extends JpaRepository<BatoipopMensaje, Integer>{
	
	@Query(value = "SELECT * FROM batoipop_denuncia_mensaje WHERE id_usuario_enviado like ?1", nativeQuery = true)
	List<BatoipopMensaje> findAllByIdUserEnviado(Integer id);
	
	@Query(value = "SELECT * FROM batoipop_denuncia_mensaje WHERE id_usuario_recibido like ?1", nativeQuery = true)
	List<BatoipopMensaje> findAllByIdUserRecibido(Integer id);

}
