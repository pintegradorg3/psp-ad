package com.batoipop.apiResBatoipop.Repository;

import org.springframework.data.jpa.repository.JpaRepository;


import com.batoipop.apiResBatoipop.pojos.BatoipopFoto;


public interface FotoRepository extends JpaRepository<BatoipopFoto, Integer>{

}
