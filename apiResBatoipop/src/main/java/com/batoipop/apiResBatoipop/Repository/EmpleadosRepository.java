package com.batoipop.apiResBatoipop.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.batoipop.apiResBatoipop.pojos.BatoipopEmpleados;

public interface EmpleadosRepository extends JpaRepository<BatoipopEmpleados, Integer>{
	
	@Query(value = "select * from batoipop_empleados be where be.correo_empleado = ?1", nativeQuery = true)
	BatoipopEmpleados findByCorreoEmpleado(String email);

}
