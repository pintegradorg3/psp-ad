package com.batoipop.apiResBatoipop.Repository;



import org.springframework.data.jpa.repository.JpaRepository;


import com.batoipop.apiResBatoipop.pojos.BatoipopCategoria;


public interface CategoriaRepository extends JpaRepository<BatoipopCategoria, Integer>{

}
