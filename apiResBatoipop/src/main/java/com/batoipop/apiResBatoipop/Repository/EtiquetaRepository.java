package com.batoipop.apiResBatoipop.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.batoipop.apiResBatoipop.pojos.BatoipopEtiqueta;

public interface EtiquetaRepository extends JpaRepository<BatoipopEtiqueta, Integer>{
	
	@Query(value = "SELECT * FROM batoipop_etiqueta WHERE description = ?1", nativeQuery = true)
	BatoipopEtiqueta findByDescripcion (String description);

}
