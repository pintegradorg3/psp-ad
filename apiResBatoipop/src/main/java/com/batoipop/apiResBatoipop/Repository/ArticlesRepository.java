package com.batoipop.apiResBatoipop.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.batoipop.apiResBatoipop.pojos.BatoipopArticles;


public interface ArticlesRepository extends JpaRepository<BatoipopArticles, Integer>{
	
	// Articulo por categoria
	@Query(value = "SELECT * FROM batoipop_articles WHERE categoria like ?1", nativeQuery = true)
	List<BatoipopArticles> findAllByIdCategoria(Integer id); 
	
	// Articulo por usuario que vende
	@Query(value = "SELECT * FROM batoipop_articles WHERE usuario_publica_articulo like ?1", nativeQuery = true)
	List<BatoipopArticles> findAllByIdUsuario(Integer id); 
	
	// Articulos por población
	@Query(value = "select ba.* from batoipop_articles ba inner join batoipop_usuario bu on ba.usuario_publica_articulo = bu.id where bu.poblacion like ?1", nativeQuery = true)
	List<BatoipopArticles> findAllPoblacion(String poblacion); 
	
	// Articulos por precio	
	@Query(value = "select * from batoipop_articles ba where ba.precio_articulo <= ?1", nativeQuery = true)
	List<BatoipopArticles> findAllByPrecioMAX(Double precio); 
	
	// Articulos por disponible
	@Query(value = "select * from batoipop_articles ba where ba.disponible = ?1", nativeQuery = true)
	List<BatoipopArticles> findAllByDisponible(); 
}
