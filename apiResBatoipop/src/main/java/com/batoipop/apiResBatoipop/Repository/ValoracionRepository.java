package com.batoipop.apiResBatoipop.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.batoipop.apiResBatoipop.pojos.BatoipopValoracion;

public interface ValoracionRepository extends JpaRepository<BatoipopValoracion, Integer> {

		@Query(value = "select * from batoipop_valoracion bv where bv.usuario = ?1", nativeQuery = true)
		List<BatoipopValoracion> findAllByUser(Integer id);
	
}
