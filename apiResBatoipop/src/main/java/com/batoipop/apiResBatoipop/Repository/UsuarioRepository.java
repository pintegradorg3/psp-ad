package com.batoipop.apiResBatoipop.Repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.batoipop.apiResBatoipop.pojos.BatoipopUsuario;

public interface UsuarioRepository extends JpaRepository<BatoipopUsuario, Integer>{
	
	
	@Query(value = "SELECT * FROM batoipop_usuario WHERE codigo_postal = ?1", nativeQuery = true)
	List<BatoipopUsuario> findByCP(String codigoPostal);
	
	@Query(value = "SELECT * FROM batoipop_usuario WHERE poblacion like %?1", nativeQuery = true)
	List<BatoipopUsuario> findByPoblacion(String poblacion);
	
	@Query(value = "SELECT * FROM batoipop_usuario WHERE correo_usuario = ?1", nativeQuery = true)
	BatoipopUsuario findByCorreoUsuario(String email);
}
