package com.chatserver;
import javax.crypto.*;

import javax.crypto.spec.IvParameterSpec;
import javax.swing.*;
import java.awt.*;
import java.io.*;

import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {

    private final static String CLAVE = "Alex Alonso Erian Victoria", SALT = "Batoipop", ALGORITHM = "AES/CBC/PKCS5Padding", IV = "hola caracola 12";
    private static ArrayList<UsuariosOnLine> usuariosOnLine;
    private final static int PORT = 9098;
    public static void main(String[] args) {
        play();
    }

    private static void play() {
        usuariosOnLine = new ArrayList<>();

        ExecutorService executorService = Executors.newSingleThreadExecutor();

        while (true) {
            executorService.execute(new Runnable() {
                @Override
                public void run() {
                    try (ServerSocket serverSocket = new ServerSocket(PORT)) {
                        System.out.println("Número de conexiones: "+ usuariosOnLine.size());
                        Socket socket;
                        SecretKey secretKey = AESCifrado.getKeyFromPassword(CLAVE, SALT);
                        IvParameterSpec iv = new IvParameterSpec(IV.getBytes(StandardCharsets.UTF_8));

                        // while (true) {
                        socket = serverSocket.accept();
                        /*
                         * Recibir datos
                         */
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                        String idArticulo = bufferedReader.readLine();
                        String nameUsuario = bufferedReader.readLine();

                        UsuariosOnLine usuario = anyadirUsuarios(Integer.parseInt(idArticulo), nameUsuario, socket);
                        assert usuario != null;
                        //        System.out.println(usuario.nick + " " + usuario.idArticulo + " " + usuario.ip);

                        String mensajeCifrado = bufferedReader.readLine();
                        String mensajeDescifrado = AESCifrado.decrypt(ALGORITHM, mensajeCifrado, secretKey, iv);

                        /*
                         * Mostrar texto en terminal
                         */
                        System.out.println(" * " + usuario.nick + " " + mensajeDescifrado);

                        /*
                         * Parte para reenviar al cliente el paquete
                         */

                        // Si solo hay un usuario conectado no se pueden enviar mensajes, asique los guardamos en el usuario
                        if (usuariosOnLine.size() > 1) {
                            UsuariosOnLine clienteReenvio = usuario.buscarCliente(usuariosOnLine);
                            Socket enviarDestinatario = clienteReenvio.socket;

                            // Cuando se inicia la comunicación con el usuario adecuado se comprueban el número de mensajes guardados
                            if (clienteReenvio.mensaje.size() <= 1) {
                                enviar(clienteReenvio.nick, mensajeCifrado, clienteReenvio.socket);
                            } else { // Si solo hay uno se envía uno, si hay más se inicia un bucle que envíe todos los mensajes. Luego borramos la lista de mensajes
                                for (int i = 0; i < clienteReenvio.mensaje.size(); i++) {
                                    enviar(clienteReenvio.nick, clienteReenvio.mensaje.get(i), clienteReenvio.socket);
                                }
                                clienteReenvio.mensaje.clear();
                            }
                        } else {
                            usuario.setMensaje(mensajeCifrado);
                            enviar("Batoipop",AESCifrado.encrypt(ALGORITHM, "No hay usuarios conectados", secretKey, iv), socket);
                        }

                        // }
                    } catch (IOException e) {
                        System.out.println("ERROR AL CREAR EL SOCKET DEL SERVIDOR -> " + e.getMessage());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    /**
     * Método que nos permite comprobar los usuarios existentes antes de añadir uno nuevo
     *
     * @param id
     * @param nameUsuario
     * @param socket
     * @return
     */
    private static UsuariosOnLine anyadirUsuarios(int id, String nameUsuario, Socket socket) {
        UsuariosOnLine us = null;
        if (usuariosOnLine != null) {
            if (usuariosOnLine.size() == 0) {
                us = new UsuariosOnLine(id, nameUsuario, socket);
                usuariosOnLine.add(us);
                return us;
            } else {
                for (int i = 0; i < usuariosOnLine.size(); i++) {
                    if (!usuariosOnLine.get(i).nick.equals(nameUsuario) && usuariosOnLine.get(i).idArticulo != id && !usuariosOnLine.get(i).socket.equals(socket)) {
                        us = new UsuariosOnLine(id, nameUsuario, socket);
                        usuariosOnLine.add(us);
                        return us;
                    } else {
                        us = usuariosOnLine.get(i);
                        return us;
                    }
                }
            }
        } else {
            us = new UsuariosOnLine(id, nameUsuario, socket);
            usuariosOnLine.add(us);
            return us;
        }
        return us;
    }

    private static void enviar(String nombreUsuario, String mensaje, Socket socket) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        bufferedWriter.write(nombreUsuario);
        bufferedWriter.newLine();
        bufferedWriter.write(mensaje);
        bufferedWriter.newLine();
        bufferedWriter.flush();

    }


    /**
     * Clase creada para almacenar los usuarios que se conecten al servidor
     */
    static class UsuariosOnLine {
        int idArticulo;
        String nick;
        Socket socket;
        ArrayList<String> mensaje;

        public void setMensaje(String mensaje) {
            this.mensaje.add(mensaje);
        }


        public UsuariosOnLine(int idArticulo, String nick, Socket socket) {
            this.idArticulo = idArticulo;
            this.nick = nick;
            this.socket = socket;
            this.mensaje = new ArrayList<>();
        }

        public UsuariosOnLine buscarCliente(ArrayList<UsuariosOnLine> list){
            for (UsuariosOnLine usuarioOnLine: list ) {
                if(idArticulo == usuarioOnLine.idArticulo && !Objects.equals(nick, usuarioOnLine.nick)){
                    return usuarioOnLine;
                }
            } return null;
        }
    }

}
